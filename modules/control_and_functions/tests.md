# Вопросы по теме «Управляющие конструкции и функции»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Какое количество итераций будет выполнено в данном примере?

	```cpp
	int i {5};
	for (; i<6; i++) {
	    std::cout << i << std::endl;
	}
	```

2.	Какая из веток условия будет выполнена в данном примере?

	```cpp
	bool a = true;
	bool b = false;
    
	if ((a && b) || (b && a)) {
	    std::cout << "1" << std::endl;
	}
	else {
	    std::cout << "2" << std::endl;
	}
	```

3.	Что будет выведено на экран?

	```cpp
	for (int i {0}; i >= 0; i++) {
	    i -= 2;
	    std::cout << i << std::endl;
	}
	```

4.	Что будет выведено на экран?

	```cpp
	int value {10};
	if (value >= 10) {
	    if (value % 2 == 0) {
	        std::cout << "2" << std::endl;
	    }
	    else {
	        std::cout << "3" << std::endl;
	    }
	}
	else {
	    if (value % 3 == 0) {
	        std::cout << "1" << std::endl;
	    }
	    else {
	        std::cout << "4" << std::endl;
	    }
	}
	```

5.	Сколько раз выполнится цикл, объявленный следующим образом?

	```cpp
	for (int i {100};;) {
	    std::cout << i << std::endl;
	}
	```

6.	Какое значение будет напечатано после выполнения следующего кода:

	```cpp
	#include <iostream>

	int main() {
	    float velocity = 0;
	    float gravity = 9.8;
	    float time = 0;
	    while (time < 100) {
	        time += 1;
	        if (velocity < 50) continue;
	        velocity = gravity * time;
	    };
	    std::cout << velocity << std::endl;
	}
	```

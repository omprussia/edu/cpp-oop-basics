# Контейнеры

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Основные контейнеры и их применение. Итераторы.

Стандартная библиотека С++ предоставляет большое количество различных типов. В библиотеке существуют специальные типы, называемые контейнерами, и связанное с ними понятие "итераторы".
Контейнеры позволяют организовать хранение данных с различной организацией в памяти и разными способами обращения к элементам:

*	`array` — альтернатива простому массиву `[]`, предоставляет дополнительные методы;
*	`vector` — динамически изменяемый массив;
*	`list` — структура данных с быстрой вставкой элементов в конец;
*	`stack` — контейнер с доступом LIFO (последним зашел, первым вышел), реализующий структуру данных "стек";
*	`queue` — контейнер с доступом FIFO (первым зашел, первым вышел), реализующий структуру данных "очередь";
*	`set` — множество;
*	`map` — ассоциативный массив, контейнер для хранения пар <ключ, значение>.


## [array]

Контейнер `array<T, N>` — это фиксированная последовательность из `N` элементов заданного типа `T`:

```cpp
#include <array>

std::array<int, 5> array {1,2,3,4,5};
array[0] = 10;
std::cout << array[0] << std::endl;
```

Массив должен быть инициализирован, иначе в его элементах будут "мусорные" значения. Массив поддерживает нулевую инициализацию `{}`. Доступ к значениям `std::array` осуществляется при помощи оператора `[]`. Класс `std::array` предоставляет некоторые дополнительные методы для удобства работы с ним:

```cpp
array.fill(-1); // заполнить массив конкретным значением

std::cout << "Size of array = " << array.size() << std::endl; // получить размер массива

std::array<float, 0> brray;
std::cout << std::boolalpha << brray.empty() << std::endl; //проверить массив на отсутствие элементов
```

В классе перегружены логические операторы и оператор присваивания. Используя оператор присваивания, можно скопировать все значения одного массива в другой:

```cpp
std::array<int, 5> array {1,2,3,4,5};
std::array<int, 5> brray {2,3,4,5,6};

std::cout << std::boolalpha << (array < brray) << std::endl;

array = brray;

std::cout << std::boolalpha << (array == brray) << std::endl;
```

## [vector]

Контейнер `std::vector<T>` хранит последовательность однотипных значений. В отличие от массива размер вектора может динамически меняться во время выполнения программы. Выделение памяти для новых элементов происходит автоматически. При создании вектора нет необходимости передавать размер:

```cpp
#include <vector>

std::vector<double> vec1 (3); // вектор, инициализированный тремя нулями
std::cout << vec1.size() << std::endl;

std::vector<int> vec2 {10, 12, 14}; // инициализация вектора тремя значениями
std::cout << vec2.size() << std::endl;

std::vector<short> vec3; // пустой вектор
std::cout << vec3.size() << std::endl;

std::vector<short> vec4 (10, 42); // вектор из 10 значений равных 42
std::cout << vec4.size() << std::endl;
```

Доступ к элементам осуществляется через оператор `[]`. 

```cpp
std::cout << vec2[0] << std::endl;
```

После создания вектора в него может быть добавлено любое (но ограниченное памятью) количество элементов:

```cpp
std::vector<int> vec;

vec.push_back(10);
vec.push_back(20);
vec.push_back(30);

for (auto value: vec) {
    std::cout << value << std::endl;
}
```

Метод `push_back` добавляет новые элементы в конец вектора. 
В отличие от массива, для которого память выделяется один раз и очищается только при удалении массива, вектор может быть либо очищен от всех элементов с помощью  `clear`, либо из него может быть извлечен последний элемент методом `pop_back`:

```cpp
std::vector<int> vec {10, 20, 30}; // инициализация вектора тремя значениями

std::cout << vec.capacity() << std::endl;

vec.pop_back();
vec.pop_back();
vec.pop_back();

std::cout << vec.size() << std::endl;

vec.reserve(100);

std::cout << vec.capacity() << std::endl;
```

Метод `capacity` у вектора позволяет узнать, на какое количество элементов зарезервирована память, а метод `reserve` позволяет зарезервировать определенное количество памяти.

У класса "вектор" перегружены логические операторы. Допускается сравнивать вектора:

```cpp
std::vector<int> vec1 {10, 20, 30}; // инициализация вектора тремя значениями

std::vector<int> vec2 {20, 30};

std::cout << std::boolalpha << (vec1 < vec2) << std::endl;
```

Для сравнения используется лексикографический подход. При этом сравниваются не только сами элементы, но и их количество.

## [list]

Контейнер `std::list` хранит значения в двусвязном списке:

![Процедурная концепция](../../resources/double_linked_list.svg)

Каждый элемент двусвязного списка хранит указатели на предыдущий и на следующий элементы. Это позволяет очень быстро осуществлять вставку в произвольное место. Достаточно "разорвать" указатели и присвоить новые адреса. Доступ к случайным элементам происходит медленнее, чем при использовании массива или вектора, так как адрес элемента неизвестен и находится в случайной области памяти.

Интерфейс списка отличается от вектора; например, отсутствует возможность обращения к элементам при помощи оператора `[]`. Элементы могут быть добавлены в любое место:

```cpp
std::list<int> list;
std::list<int> vlist {1, 2, 3, 4};

list.push_back(10);
list.push_front(0);
list.insert(list.begin(), -1);
list.insert(list.end(), -2);
list.insert(list.begin(), vlist.begin(), vlist.end());

for (auto value: list) {
    std::cout << value << std::endl;
}
```

Произвольная вставка требует указания места, куда конкретно будут добавлены элементы. Для этого используются указатели `begin()` и `end()`, которые возвращают указатели на первый и последний элемент соответственно. Получение элемента по определенному индексу в списке невозможно. Чтобы получить определенный элемент, требуется пройти весь список и найти искомое. Например, для получения 2 и 3 элемента необходимо создать итератор:

```cpp
#include <iterator>

std::list<int>::iterator it = list.begin();
it++;

std::cout << *it << std::endl;

it++;
std::cout << *it << std::endl;
```

Простой доступ к элементам списка доступен только для первого и последнего элемента, т.к. в двусвязном списке всегда известны указатели на начало и конец:

```cpp
std::cout << *list.begin() << std::endl;
std::cout << *(--list.end()) << std::endl;
```

Удалять элементы можно как в произвольных местах при помощи метода `remove`, так и с начала или конца:

```cpp
list.pop_back(); // удалить элемент с конца
list.pop_front(); // удалить элемент с начала

list.remove(4); // удаление всех элементов, которые равны 4
```

### Итератор

Итераторы — специальные объекты, которые позволяют перебирать элементы любого контейнера. Итератор всегда имеет тип контейнера и тип данных, которые хранит этот контейнер:

```cpp
std::vector<double> vec {1.5, 3.5, 5.0, 8.5};
std::vector<double>::iterator it {vec.begin()};

std::cout << *it << std::endl;
std::cout << *(it + 1) << std::endl;
```

Метод `begin` возвращает итератор, который указывает на первый элемент. Для перемещения по элементам используется арифметика указателей:

```cpp
std::cout << *(++it) << std::endl;
std::cout << *(--it) << std::endl;
```

Итераторы можно использовать для обхода всех элементов в цикле:

```cpp
std::vector<int> vec {1, 2, 3, 4, 5};

for (std::vector<int>::iterator it {vec.begin()}; it != vec.end(); it++) {
    std::cout << *it << std::endl;
}
```

Итераторы позволяют объединять значения из разных контейнеров. Например, для расширения списка можно использовать элементы вектора:

```cpp
std::vector<int> vec {1, 2, 3, 4, 5};
std::list<int> list {6, 7, 8};

list.insert(list.begin(), vec.begin(), vec.end());

for(auto value: list){
    std::cout << value << std::endl;
}
```

Используя итератор, можно модифицировать значения:

```cpp
std::vector<int> vec {1, 2, 3, 4, 5};
std::vector<int>::iterator it {vec.begin()};

for (; it != vec.end(); it++) {
    *it = *it * 2;
}

it = vec.begin(); //сброс итератора

for (; it != vec.end(); it++) {
    std::cout << *it << std::endl;
}
```

Итератор позволяет сделать только один проход по элементам. Для повторного использования итератора необходимо сбросить его указатель.
Существует разновидность итераторов, которая запрещает изменение объектов:

```cpp
std::vector<int>::const_iterator it {vec.begin()};
```

Константный итератор разрешает только чтение элементов контейнера.

## [stack] и [queue]

Контейнеры `stack` и `queue` отличаются выдачей добавленных значений:

```cpp
std::stack<int> stack {};
std::queue<int> queue {};

for (int i{}; i<5; i++) {
    stack.push(i);
    queue.push(i);
}

for (int i{}; i<5; i++) {
    std::cout << "Stack pop: " << stack.top() << std::endl;
    stack.pop();
    std::cout << "Queue pop: " << queue.front() << std::endl;
    queue.pop();
}
```

Пример использования стека:

```cpp
bool checkBalanced(std::string brackets) {
    std::stack<char> stack {};
    
    for (char bracket: brackets) {
        if (bracket == ')') {
            if (stack.empty()) {
                return false;
            }
            stack.pop();
        }
        else {
            stack.push(bracket);
        }
    }
    return stack.empty();
}

int main() {
    std::string brackets {"(((()())(())))"};
    
    if (checkBalanced(brackets)) {
        std::cout << "Brackets are balanced" << std::endl;
    }
    else {
        std::cout << "Brackets are unbalanced" << std::endl;
    }
    
    return 0;
}
```

## [set]

Контейнер `set` представляет структуру данных "множество". Множество отличается от всех остальных контейнеров тем, что в нем могут храниться значения только в единственном экземпляре:

```cpp
std::set<int> set {};

for (int i{}; i<5; i++) {
    set.insert(1);
}

for (auto value: set) {
    std::cout << value << std::endl;
}

std::cout << "Size of set: " << set.size() << std::endl;
```

Множество можно использовать для удаления дубликатов:

```cpp
std::vector<int> vec {5, 5, 4, 4, 4, 3, 3, 3, 2, 2, 1};
std::set<int> set {};

for (auto value: vec) {
    set.insert(value);
}

for (auto value: set) {
    std::cout << value << std::endl;
}

std::cout << "Size of set: " << set.size() << std::endl;
```

Вывод:

```
1
2
3
4
5
Size of set: 5
```

В выходной последовательности видно, что значения содержатся только в единственном экземпляре и отсортированы по возрастанию. Если необходимо избежать сортировки результата, то следует воспользоваться разновидностью контейнера `unordered_set`.

Вариант сортировки для `set` задается при объявлении:

```cpp
std::set<int, std::less<int>> set {};
```

## [map]

Контейнер `map` отличается от представленных выше. Он позволяет хранить значения по уникальным ключам. Каждому уникальному ключу соответствует одно значение. Такой вид контейнера также называется ассоциативным массивом или словарем. При его объявлении необходимо задать два типа — тип для ключа и тип для значения:

```cpp
std::map<std::string, int> grades {};

grades["Vasya"] = 5;
grades["Petya"] = 2;
grades["Sasha"] = 4;
grades["Masha"] = 5;

std::cout << "Sasha's grade is " << grades["Sasha"] << std::endl;
```

Значения добавляются при помощи оператора `[]`. Вместо индекса в скобках указывается ключ типа, указанного при объявлении.

При переборе элементов возвращается пара ключ и значение:

```cpp
for (const std::pair<std::string, int>& grade: grades) {
//for (auto grade: grades) {
    std::cout << grade.first << "'s grade is " << grade.second << std::endl;
}
```

Проверка на наличие определенного ключа осуществляется при помощи метода `count`. Данный метод возвращает 1, если ключ есть, и 0, если нет:

```cpp
if (grades.count("Dasha")) {
    std::cout << "Key found!" << std::endl;
}
else {
    std::cout << "Key not found!" << std::endl;
}
```

Ассоциативные массивы широко используются, например, когда необходимо подсчитать количество чего-либо:

```cpp
std::vector<int> vec {1, 2, 1, 3, 2, 1, 3, 4, 5, 5, 5, 1, 2, 3, 4, 5};
std::map<int, int> counter {};

for (auto value: vec) {
    counter[value] += 1;
}

for (auto count: counter) {
    std::cout << count.first << " = " << count.second << std::endl;
}
```

[array]: https://en.cppreference.com/w/cpp/container/array "Контейнер array"
[vector]: https://en.cppreference.com/w/cpp/container/vector "Контейнер vector"
[list]: https://en.cppreference.com/w/cpp/container/list "Контейнер list"
[stack]: https://en.cppreference.com/w/cpp/container/stack "Контейнер stack"
[queue]: https://en.cppreference.com/w/cpp/container/queue "Контейнер queue"
[map]: https://en.cppreference.com/w/cpp/container/map "Контейнер map"

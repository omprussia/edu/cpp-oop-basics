# Расширение и ограничение поведения класса

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Абстракция, наследование и композиция. Понятия "базовый класс" и "производный класс".

Классы в объектно-ориентированном подходе позволяют уменьшить сложность задачи за счет разбиения ее на мелкие блоки. Класс описывает некоторую совокупность объектов, выделяя основные характеристики и убирая все, что неважно. Важными концепциями в ООП являются: наследование (`inheritance`), абстракция (`abstract`) и композиция  (`composition`). Данные концепции расширяют механизм классов и типов.

## Наследование

Наследование позволяет определить класс в рамках другого класса. Это, в свою очередь, позволяет переиспользовать уже написанный код и адаптировать поведение класса под нужды предметной области. При создании нового класса нет необходимости в описании всех полей и методов, класс их может наследовать от уже существующего класса. В таком случае класс, от которого наследуются поля и методы, называется родительским, а тот класс, который получает поля и классы от родительского, называется классом-наследником.

Чтобы создать класс-наследник, при объявлении класса необходимо указать родительский класс ([код](../../projects/inheritance)):

```cpp
class Enemy {
    private:
        std::string m_name = {};

    protected:
        int m_hp = 0;
    
    public:
        Enemy(){};
        Enemy(std::string name, int hp): m_name {name}, m_hp {hp}{
        }

        void display() {
            std::cout << "Enemy: " << m_name << " with hp = " << m_hp << std::endl;
        }
        
        void sleep() {
            std::cout << "Enemy: sleep"  << std::endl;
        }
};

class Mage: public Enemy {
    protected:
        int m_mp = 0;

    public:
        Mage(std::string name, int hp, int mp) : Enemy(name, hp), m_mp{mp} {
        }
};

Mage mage {"Koschei the Deathless", 20, 50};
mage.display();
```

В примере объявлены два класса: `Enemy` — базовый класс и `Mage` — класс-наследник. Класс `Mage` получает все поля и методы от родительского класса. Например, метод `display` можно просто вызвать без необходимости его объявления.

В С++ существует несколько типов наследования. Тип наследования указывается перед классом. Каждый тип отличается по способу доступа к полям и методам.

*	`public` — все публичные члены родительского класса становятся публичными членами класса-наследника. Все приватные члены недоступны напрямую классу-наследнику. Класс-наследник может получить приватные члены только при помощи публичных методов. Защищенные члены родительского класса становятся защищенными членами класса-наследника.
*	`protected` — все публичные и защищенные члены родительского класса становятся защищенными членами класса-наследника.
*	`private` — все публичные и защищенные члены родительского класса становятся приватными членами класса-наследника.

В приведенном выше примере член класса `m_hp` объявлен в защищенной области, поэтому доступен классу-наследнику без каких-либо проблем, но при этом недоступен снаружи класса. Член класса `m_name` объявлен в приватной области, поэтому к нему не получится обратиться внутри класса `Mage`:

```cpp
// Mage

int getHP() {
        return m_hp;
    }    

std::string getName() {
        return m_name; // нельзя обратиться к приватному члену класса
    }
```

За счет наследования классы могут ограничивать и расширять поведение родительского класса:

```cpp
class Mage: public Enemy {
    protected:
        int m_mp = 0;

    public:
        Mage(std::string name, int hp, int mp) : Enemy(name, hp), m_mp{mp} {
        }

        int getHP() {
            return m_hp;
        }
        
        void cast() {
            std::cout << "Mage cast a spell!" << std::endl;
        }
        
        void display() {
            std::cout << "Mage(" << m_hp << "/" << m_mp << ")" << std::endl;       
        }

        void sleep() = delete;
};
```

В этом примере добавлен новый метод `cast` и переопределена функция `display`. При помощи ключевого слова `delete` удален метод `sleep`. При попытке вызова данного метода из наследника будет выдана ошибка:

```cpp
Enemy enemy {"Orc", 60};
enemy.display();
enemy.sleep();

Mage mage {"Koschei the Deathless", 20, 50};
mage.display();
mage.sleep(); // use of deleted function ‘void Mage::sleep()’
```

Количество классов-наследников не ограничено. Также, если не указано другое, не ограничена глубина наследования:

```cpp
class Wizard: public Mage {
};
```

Ограничить глубину наследования можно при помощи ключевого слова `final`. Если данный модификатор поставить к классу `Wizard`, то от него нельзя будет создать производный класс:

```cpp
class Wizard final: public Mage {
};

class Necromant: public Wizard { // ошибка, класс Wizard объявлен как final
};
```

Наследование в С++ не ограничено одним родительским классом. Можно унаследовать поля и методы от нескольких классов:

```cpp
class Necromant{

    public:
        
        void raiseUndead() {
            std::cout << "raise a zombie!" << std::endl;
        }

};

class UndeadMage: public Mage, public Necromant{

    public:
        UndeadMage(std::string name, int hp, int mp): Mage(name, hp, mp){
        };

};
```

В данном случае класс `UndeadMage` получает доступ ко всем полям `Mage` и к методу `raiseUndead` из класса `Necromant`.

При создании конструктора для наследника необходимо учитывать конструкторы родительских классов. Если в конструкторах родительских классов происходит инициализация полей, то их необходимо вызвать при инициализации.

Класс определяет тип данных. Использование базового класса позволяет "обобщить" использование производных классов:

```cpp
class Race {
    private:
        std::string m_name;
    public:
        Race(std::string name): m_name{name} {
        };
        void display() {
            std::cout << "Race: " << m_name << std::endl;
        }
};

class Human: public Race {
    public:
        Human(): Race("Human"){
        };
};

class Dwarf: public Race {
    public:
        Dwarf(): Race("Dwarf"){
        };
};

Race races[] {Human {}, Dwarf {}};

for (auto race: races) {
    race.display();
}
```

Массив `races` объявлен как базовый тип `Race`. В этот массив могут быть добавлены все производные классы `Race`. Таким образом, массив может содержать любое число разновидностей класса `Race`. Дочерние классы являются заменой для базового класса. Однако таким же образом нельзя использовать дочерние классы, это приведет к ошибке, так как производные классы не взаимозаменяемы:

```cpp
Human races[] {Human {}, Dwarf {}}; // conversion from ‘Dwarf’ to non-scalar type ‘Human’ requested

for (Race race: races) {
    race.display();
}
```

## Абстрактные классы

Абстракция предназначена для сокрытия внутренних деталей объекта от внешнего мира. Она используется для описания объектов в простых терминах. Например, смартфон является примером такой абстракции. Смартфон можно использовать для звонков, и при этом не нужно разбираться в том, какие в нем чипы, процессор и из какого количества деталей он сделан.

В языке C++ для создания абстракций существует специальная версия классов, которая используется только в формате базового класса и не используется для создания объектов. Такие классы называются абстрактными. Абстрактный класс должен содержать как минимум одну виртуальную функцию. Виртуальная функция объявляется при помощи ключевого слова `virtual` и спецификатора  `=0`.

```cpp
class Race {
    private:
        std::string m_name;
    public:
        Race(std::string name): m_name{name} {
        };
        virtual void info() = 0;
};
```

Если попытаться создать объект такого класса, произойдет ошибка `cannot declare variable ‘race’ to be of abstract type ‘Race’`. Абстрактные классы могут использоваться только как тип данных или для наследования. Наследование не отличается от обычных классов, но требует, чтобы виртуальный метод был реализован в классе-наследнике, иначе производный класс будет так же считаться абстрактным:

```cpp
class Human: public Race {
    public:
        Human(): Race("Human"){
        };
        void info() {
            std::cout << "Size: medium, Type: humanoid, Alignment: any" << std::endl;
        };
};
```

## Композиция объектов

В реальных приложениях часто используются сложные объекты, состоящие из совокупности более простых объектов. Например, даже использование объекта класса `std::string` расширяет возможности класса за счет новых методов и способов взаимодействия. Использование объектов внутри классов называется композицией.

Существует несколько вариантов композиции:

*	композиция;
*	агрегация.

Композиция описывает класс, который ссылается на один или больше объектов других классов. Отношения между классами, входящими в композицию, можно описать следующим образом:

*	объект является частью другого объекта;
*	объект принадлежит только одному другому объекту;
*	существование объекта контролируется другим объектом;
*	объект ничего не знает о существовании другого объекта.


Пример ([код](../../projects/composition)):

```cpp
class Heater {
    private:
        float m_temperature {0};
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        Heater(){};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }
        
        int getTemperature() {
            return m_temperature;
        }

        void on() {
            m_enabled = true;
        }

        void off() {
            m_enabled = false;
        }

};

class Light {
    private:
        bool m_enabled {false};

    public:
        Light(){};
    
        void on() {
            m_enabled = true;
        }
    
        void off() {
            m_enabled = false;
        }
};

class HomeAssistant {
    private:
        Light m_light {};
        Heater m_heater {};
    
    public:
        HomeAssistant(){};
        ~HomeAssistant(){
            m_light.off();
            m_heater.off();
        };

        void arriveHome(){
            m_light.on();
            m_heater.setTargetTemperature(24);
            m_heater.on();
        }

        void leaveHome(){
            m_light.off();
            m_heater.setTargetTemperature(18);
        }
    
};
```

Использование:

```cpp
HomeAssistant ha {};
ha.arriveHome();
```

`HomeAssistant` — это композиция из двух объектов. Объекты `m_heater` и `m_light` ничего не знают о существовании объекта, в который они встроены. Объекты создаются в момент создания их общего объекта и будут уничтожены в момент, когда он перестанет существовать.

Агрегация, в отличии от композиции, допускает принадлежность объекта нескольким другим объектам. При агрегации объект, содержащий другой объект, не управляет его временем существования ([код](../../projects/aggregation)):

```cpp
class TemperatureSensor {
    private:
        float m_temperature {22.0};

    public:
        TemperatureSensor(){};
        float getTemperature(){
            return m_temperature;
        };
};


class Heater {
    private:
        TemperatureSensor* m_sensor;
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        Heater(TemperatureSensor* sensor): m_sensor{sensor}{};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }

        void on() {
            m_enabled = true;
            std::cout << "Start heating. Current temperature: " << m_sensor->getTemperature() << std::endl;
        }

        void off() {
            m_enabled = false;
        }
};

class AirConditioner {
    private:
        TemperatureSensor* m_sensor;
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        AirConditioner(TemperatureSensor* sensor): m_sensor{sensor}{};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }

        void on() {
            m_enabled = true;
            std::cout << "Start cooling. Current temperature: " << m_sensor->getTemperature() << std::endl;
        }

        void off() {
            m_enabled = false;
        }
};
```

Использование:

```cpp
TemperatureSensor sensor {};
Heater heater {&sensor};
AirConditioner conditioner {&sensor};

heater.on();
conditioner.on();
```

`sensor` является общим для двух объектов. При этом он создается снаружи, так что никакой из двух объектов не сможет его удалить. Объект может быть использован любым числом других объектов, которым нужен доступ к данным, которые он предоставляет.

Агрегация сложнее с точки зрения управления ресурсами, так как не несет ответственности за объекты, которые использует. Это может приводить к ошибкам, если общий объект будет удален.

## Приведение типов и полиморфизм

В некоторых случаях, например, при обобщении передаваемого типа, может понадобиться обратиться к методу наследуемого типа:

```cpp
class Base {
    public:
        virtual ~Base(){}
        void display() {
            std::cout << "Base class" << std::endl;
        }
};

class Child: public Base {
    public:
        void display() {
            std::cout << "Child class" << std::endl;
        }
};

void display(Base object) {
    object.display();
}

Child child;
display(child);
```

Функция `display` выведет на экран `Base class`, так как для входного аргумента указан базовый тип данных `Base`.
Для решения проблемы можно использовать пребразование вида `dynamic_cast`. `dynamic_cast` выполняется во время исполнения программы:

```cpp
void display(Base* object) {
    dynamic_cast<Child*>(object)->display();
}
```

Другим способом решения будет использование полиморфизма. Полиморфизм связан с иерархией классов и отвечает за вызов правильного метода в зависимости от типа объекта, который вызвал данный метод. Для этого достаточно дополнить метод базового класса ключевым словом `virtual`:

```cpp
virtual void display() {
    std::cout << "Base class" << std::endl;
}
```

Объявление хотя бы одного метода со словом `virtual` делает тип данных полиморфным. Виртуальная функция указывает компилятору, что функция не задана статически и может отличаться в зависимости от вызывающего ее объекта.

# Паттерны и их назначение

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Переиспользование кода. Классификация паттернов и их применение.

Программистами решено большое количество задач и написано еще больше кода. Возникающие в ходе решения задач архитектурные решения во многом схожи и могут быть переиспользованы многократно. "Стандартные" решения представлены в виде шаблонов ([patterns]) кода, которые могут использоваться в разных предметных областях и на разных языках программирования.

Шаблоны можно разделить по задачам, которые они решают. Какие-то шаблоны помогают создавать другие объекты, какие-то — задавать логическую последовательность или порядок действий.

Согласно категоризации, придуманной "Бандой Четырех" (Эрих Гамма, Ричард Хелм, Ральф Джонсон, Джон Влиссидес), паттерны можно разделить на несколько видов:

*	порождающие паттерны:
  * абстрактная фабрика (Abstract Factory);
  * одиночка (Singleton);
  * строитель (Builder).
*	структурные паттерны:
  * адаптер (Adapter);
  * декоратор (Decorator);
  * приспособленец (Flyweight).
*	поведенческие паттерны:
  * наблюдатель (Observer);
  * состояние (State);
  * стратегия (Strategy);
  * команда (Command).

Каждый паттерн предназначен для решения какой-то конкретной проблемы, которая возникает при решений задач программирования.

Хорошим подходом при создании фреймворка/библиотеки является использование известных шаблонов и интерфейсов, это позволяет пользователям фреймворка легко понимать, какие объекты для чего используются, и применять их эффективно.

Например, в составе библиотеки `Qt` имеются классы, которые полагаются на тот или иной шаблон:

*	`QApplication`(`QConsoleApplication`) — шаблон Singleton;
*	`QEvent` — шаблон Observer;
*	`QAction` — шаблон Command;
*	`QState` — шаблон State.

Примеры кода:

```cpp
QStateMachine machine = new QStateMachine;

QState *inputState = new QState(machine);
inputState->assignProperty(this, "status", "Move the rogue with 2, 4, 6, and 8");

QState *quitState = new QState(machine);
quitState->assignProperty(this, "status", "Really quit(y/n)?");

machine->setInitialState(inputState);
machine->start();
```

На основе `QStateMachine` реализуется логика поведения виджетов (графических элементов пользовательского интерфейса).

`QEvent` используется для описания возможных событий и реакций на них. Например, список событий для одного класса `QLineEdit`:

*	`changeEvent(QEvent *ev)` — реакция на изменения состояния объекта;
*	`keyPressEvent(QKeyEvent *event)` — реакция на нажатие клавиши;
*	`keyReleaseEvent(QKeyEvent *)` — реакция на отжатие клавиши;
*	`mouseDoubleClickEvent(QMouseEvent *e)` — реакция на двойное нажатие мыши;
*	`mouseMoveEvent(QMouseEvent *e)` — реакция на перемещение курсора мыши;
*	`mousePressEvent(QMouseEvent *e)` — реакция на нажатие клавиши мыши;
*	`mouseReleaseEvent(QMouseEvent *e)` — реакция на отжатие клавиши мыши.

На каждое событие можно установить какой-либо обработчик, который будет вызван автоматически. На одно событие можно установить несколько подписчиков, каждый из которых будет вызван при появлении события.

Класс `QCoreApplication` представляет собой пример паттерна `Singleton`:

```cpp
QCoreApplication a(argc, argv);
a.setApplicationName("Singleton");

qDebug() << a.applicationName();
qDebug() << &a;

QCoreApplication* b = QCoreApplication::instance();
qDebug() << b->applicationName();
qDebug() << b;
```

Вывод:

    "Singleton"
    QCoreApplication(0x7ffeedde3a40)
    "Singleton"
    QCoreApplication(0x7ffeedde3a40)


[patterns]: https://en.wikipedia.org/wiki/Design_Patterns "Приемы объектно-ориентированного проектирования. Паттерны проектирования"

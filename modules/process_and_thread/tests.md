# Вопросы по теме «Процесс и поток»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Верно ли утверждение, что при выполнении четырех задач на четырех ядрах процессора можно получить четырехкратное увеличение производительности?
2.	Что необходимо, чтобы код программы выполнялся параллельно?
3.	Верно ли, что одному процессу всегда соответствует не менее одного потока?
4.	Какую память делят между собой потоки?

# Паттерн Singleton

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Реализация паттерна на С++. Пример использования.

`Singleton` или одиночка — это специальный класс, который позволяет иметь только один объект. Любая созданная вновь переменная должна ссылаться на ранее созданный объект. Это может быть необходимо для разделяемых ресурсов при подключении к базе данных, записи в один файл из разных мест, серийном подключении или подключении к Web-камере.

Основные преимущества использования одиночки:
*	полный контроль над доступом;
*	в отличие от переменных, не засоряет глобальное пространство имен;
*	позволяет контролировать количество объектов, может быть несколько одиночек одного класса.

```cpp
class Singleton {

    public:
        static Singleton* getInstance();

    protected:
        Singleton(){};

    private:
        static Singleton* _instance;
    
};

Singleton* Singleton::_instance = nullptr;

Singleton* Singleton::getInstance(){
    if (_instance == nullptr) {
        _instance = new Singleton;
    }
    return _instance;
}

int main() {
    //Singleton singleton {};
    
    Singleton* singleton1 = Singleton::getInstance();
    Singleton* singleton2 = Singleton::getInstance();
    
    std::cout << singleton1 << std::endl;
    std::cout << singleton2 << std::endl;
    
    return 0;
}
```

Вывод:

```
0x1001a68
0x1001a68
```

Поведение одиночки реализуется при помощи статического поля `_instance`, которое хранит единственный экземпляр класса `Singleton`. Чтобы не было возможности создать еще один объект, конструктор выносится в защищенную область. Из-за этого невозможно создать объект снаружи класса. Единственный способ получения доступа к одиночке — метод `getInstance`. Этот метод при первом обращении проверяет, что статическое поле `_instance` задано как `nullptr`, и создает объект. При следующих обращениях, так как поле уже не равно `nullptr`, новый объект не создается, а возвращается всегда ранее созданный.

Пример использования:

```cpp
const char* fileName {"log.txt"};

class Logger {

    public:
        ~Logger(){
            m_out.close();
        }
        static Logger* getInstance();
        void info(std::string message);
        void debug(std::string message);

    protected:
        Logger(){
            m_out.open(fileName, std::ios_base::app);
        };

    private:
        static Logger* _instance;
        std::ofstream m_out;
    
};

Logger* Logger::_instance = nullptr;

Logger* Logger::getInstance(){
    if (_instance == nullptr) {
        _instance = new Logger;
    }
    return _instance;
}

void Logger::info(std::string message) {
    m_out << "INFO: " << message << std::endl;
}

void Logger::debug(std::string message) {
    m_out << "DEBUG: " << message << std::endl;
}

int prod(int* array, int size) {
    int result = 1;
    
    Logger::getInstance()->debug("Called function prod");
    
    for (int i{}; i<size; i++) {
        result *= array[i];
    }
    
    return result;
}

int sum(int* array, int size) {
    int result = 0;
    
    Logger::getInstance()->debug("Called function sum");
    
    for (int i{}; i<size; i++) {
        result += array[i];
    }
    
    return result;
}


int main() {    
    Logger::getInstance()->info("Programm started!");
    int array[] {1, 2, 3, 4, 5};
    
    std::cout << prod(array, 5) << std::endl;
    std::cout << sum(array, 5) << std::endl;

    Logger::getInstance()->info("Programm done!");
    return 0;
}
```

В приведенном примере реализован класс `Logger` для логирования сообщений в файл. Класс `Logger` является одиночкой, это позволяет использовать объект в любом месте без передачи в виде параметра и контролировать доступ к файлу, что дает возможность избежать проблем с одновременной записью из разных мест. При первом создании файл для логов открывается один раз и остается доступным, пока объект класса `Logger` не будет удален.

У шаблона "одиночка" есть некоторые проблемы. Например, если с одиночкой что-то случится, весь код, который его использует, может работать неправильно. В приведенном примере может быть проблема с открытием файла, из-за этого ничего не будет записываться во всех местах, где вызываются методы одиночки. Также шаблон повышает связанность кода: если изменится интерфейс одиночки, его придется менять по всему коду.

# Объектное мышление

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Объектное мышление. Назначение ООП.

Объектно-ориентированное программирование легко ложится на мышление человека. Человек мыслит абстракциями и образами. Например, легко можно представить процесс покупки хлеба в магазине: существует некоторый объект "магазин", который в себе содержит объекты "хлеб" и "продавец". Если продавцу передать "сообщение" в виде "денег", можно получить объект "хлеб" в свое владение. При этом не важны такие свойства объекта "продавец", как возраст, пол, рост, вес, цвет волос. Главное, что у него есть действие "продать хлеб", которым можно воспользоваться.

Таким образом, взаимодействие в реальном мире происходит за счет передачи сообщений между объектами, вовлеченными в некий процесс. "Продавец" по-разному взаимодействует с покупателем и своим начальником, наборы допустимых действий отличаются. Такой набор допустимых значений в программе называется интерфейсом. Если "покупатель" попросит "продавца" поиграть с ним в футбол, "продавец" откажет ему, так как это не входит в должностные инструкции "продавца". Несмотря на то, что "продавец" может уметь играть в футбол, ему запрещено это делать на рабочем месте — происходит сокрытие некоторых взаимодействий для доступа. Если "хлеба" нет на полке, "покупатель" не имеет доступа к "складу" и не может самостоятельно зайти на склад и взять его, а "продавец" может: у него есть доступ к "складу". Это так называемая инкапсуляция — сокрытие некоторой части интерфейса для несанкционированного доступа. У "покупателя" есть имя, возраст, номер телефона, количество наличных денег — поля объекта.

Программы, представленные в предыдущих разделах, не содержат взаимодействия объектов. Они описывают некоторый алгоритм, например, вывода на экран или заполнения массива. К объектному подходу больше всего подходит программа, использующая структуры. В ней существует тип данных `city`, который описывает сущность `город`, однако нет других сущностей, которые бы пересылали сообщения городу или взаимодействовали каким-либо другим способом.

Подход, в котором используется решение, основанное на взаимодействии объектов, называется объектно-ориентированным программированием. Для его реализации в С++ присутствует механизм классов и объектов.

ООП позволяет разбить сложную задачу на набор небольших абстракций с интерфейсами взаимодействия. Абстракция — концепт объекта без лишних деталей. Например, для запуска машины достаточно повернуть ключ, необязательно знать, как она устроена внутри. А звонок по телефону не требует знания, каким образом устроена программа для проведения звонков.

Основными строительными блоками ООП являются:

*	классы — пользовательские типы данных, шаблоны отдельных объектов;
*	объекты — воплощения класса с реальными данными; объекты могут представлять реально существующие объекты, например, людей, машины, предметы, а могут и совершенно абстрактные вещи, например, графический примитив, итератор, поток;
*	методы — функции, принадлежащие классам и определяющие поведение объекта;
*	поля (атрибуты, члены класса) — состояния объекта.

Основные принципы ООП:

*	инкапсуляция — сокрытие реализации и состояния;
*	абстракция — выделение "важных" для предметной области свойств объекта;
*	полиморфизм — способность объекта принимать разные формы в зависимости от текущей ситуации;
*	наследование — переиспользование кода и расширение или ограничение родительского класса классом—наследником.

Преимущества объектно-ориентированного подхода:

*	связь с предметной областью;
*	модульность — программа делится на небольшие блоки, которые легко поддерживать и изменять;
*	защищенность критических данных — ограничение доступа;
*	упрощение совместной разработки.

# Вопросы по теме «Первая программа и основные термины»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Какая последовательность вывода будет для представленного ниже Makefile?

	```make
	all: target1
	    @echo all

	target1: target3 target2
	    @echo target1

	target2:
	    @echo target2

	target3:
	    @echo target3
	```

2.	Можно ли выполнить объектный файл (файл с расширением `.o`)?
3.	Исправить цель `all` так, чтобы при ее выполнении собиралась программа:

	```cpp
	//main.cpp
	#include <iostream>

	int main() {
	    std::cout << "Done" << std::endl;
	    return 0;
	}
	```

	Makefile

	```make
	all:
	    @echo all

	build: cpp
	    g++ -o main main.cpp
	```

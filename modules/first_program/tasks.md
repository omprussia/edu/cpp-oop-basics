# Задания по теме «Первая программа и основные термины»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Написать `Makefile` для сборки двух программ одновременно при помощи цели `all`. Для сборки каждой из этих программ должны быть написаны отдельные цели.

Код программ:

```cpp
//program1.cpp

#include <iostream>

int main() {
    std::cout << "Program 1" << std::endl;
    return 0;
}

//program2.cpp

#include <iostream>

int main() {
    std::cout << "Program 2" << std::endl;
    return 0;
}
```

# Выносим вычисления в отдельный поток

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Параллельное выполнение кода. Ускорение вычислений. Возврат результата.

Создание потока осуществляется при помощи класса `std::thread`:

```cpp
std::thread t;
```

В поток необходимо поместить код, который будет выполняться отдельно от основного (для сборки примера необходимо указать `-lpthread`):

```cpp
void function() {
    for(int i{}; i<5; i++) {
        std::cout << "Thread_" << std::this_thread::get_id()<< " i=" << i << std::endl;
    }
}

std::thread t1{function};
std::thread t2{function};

t1.join();
t2.join();
```

Вывод:

```
Thread_140258642646784 i=0
Thread_140258642646784 i=1
Thread_140258642646784 i=2
Thread_140258634254080 i=0
Thread_140258634254080 i=1
Thread_140258634254080 i=2
Thread_140258634254080 i=3
Thread_140258634254080 i=4
Thread_140258642646784 i=3
Thread_140258642646784 i=4
```

Одним из вариантов передачи кода является создание функции, которая ничего не принимает и ничего не возвращает, и передача ее при инициализации потока. Функция `std::this_thread::get_id` возвращает идентификатор для того потока, в котором был произведен вызов данной функции.
Метод `join` используется для ожидания завершения работы потока. Вызов `join` является блокирующим, код, расположенный ниже, не будет выполнен до завершения потока. Если его не использовать, программа будет завершена с ошибкой. Созданные потоки запускаются автоматически после инициализации.

Параметры для функции необходимо передавать в момент инициализации:

```cpp
void function(int min, int max) {
    for(int i{min}; i<max; i++) {
        std::cout << "Thread_" << std::this_thread::get_id()<< " i=" << i << std::endl;
    }
}

std::thread t1{function, 0, 5};
std::thread t2{function, 10, 20};
```

Другим способом передачи кода является создание класса с перегруженным оператором `()`:

```cpp
class Task {
    private:
        int m_min {};
        int m_max {};

    public:
        Task(int min, int max): m_min{min}, m_max{max} {
        }
        
        void operator()(){
            for(int i{m_min}; i<m_max; i++) {
                std::cout << "Thread_" << std::this_thread::get_id()<< " i=" << i << std::endl;
            }
        }

};

Task task1 {0, 5};
Task task2 {100, 105};

std::thread t1{task1};
std::thread t2{task2};

t1.join();
t2.join();
```

Вывод:

```
Thread_139633695364864 i=0
Thread_139633695364864 i=1
Thread_139633695364864 i=2
Thread_139633695364864 i=3
Thread_139633695364864 i=4
Thread_139633686972160 i=100
Thread_139633686972160 i=101
Thread_139633686972160 i=102
Thread_139633686972160 i=103
Thread_139633686972160 i=104
```

При выводе в стандартный поток некоторые сообщения могут ломаться, так как вывод в него происходит одновременно из двух потоков.

Чтобы получить значение из потока, можно воспользоваться изменением переменной через указатель:

```cpp
void triple(int* value) {
    std::cout << "Thread_" << std::this_thread::get_id() << std::endl;
    *value = *value * 3;
}


int value = 10;
std::thread t{triple, &value};
t.join();
std::cout << value << std::endl;
```

Измерив время выполнения кода, можно определить, насколько он был ускорен ([код](../../projects/speedup)):

```cpp
void sleep() {
    std::this_thread::sleep_for(1s);
}

int main() {
    const int nthreads {4};
    std::thread threads[nthreads];

    auto start {std::chrono::high_resolution_clock::now()};
    
    for (int i{0}; i < nthreads; i++) {
        sleep();
    }

    auto stop {std::chrono::high_resolution_clock::now()};

    std::chrono::duration<double, std::milli> elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";


    start = std::chrono::high_resolution_clock::now();

    for (int i{0}; i < nthreads; i++) {
        threads[i] = std::thread {sleep};
    }
    
    for (int i{0}; i < nthreads; i++) {
        threads[i].join();
    }
    
    stop = std::chrono::high_resolution_clock::now();
    elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    return 0;
}
```

Программа выводит время выполнения функции `sleep` при линейном исполнении и при использовании потоков. Например, вывод для `nthreads = 4`:

```
4000.32ms
1000.34ms
```

### Пример задачи для ускорения с использованием потоков

Дана задача: найти треугольники с максимальной площадью по заданным наборам точек. Наборы точек заданы при помощи файлов. Каждый файл содержит 250 точек. Точка описывается координатами (x, y). Координаты находятся в диапазоне от -100 до 100. Каждая пара координат находится в новой строке.

Решение задачи перебором:

```cpp
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <thread>
#include <chrono>
#include <vector>
#include <cmath>

using namespace std::chrono_literals;


std::vector<std::pair<int, int>> loadFile(std::string fileName) {
    std::ifstream fin;
    fin.open(fileName);
    if (!fin.is_open()) {
        throw "Can't load file";
    }

    std::vector<std::pair<int, int>> coordinates {};

    std::string line {};
    std::pair<int, int> point;
    std::istringstream sline {};

    while (fin) {
        std::getline(fin, line);
        sline.clear();
        sline.str(line);
        sline >> point.first;
        sline >> point.second;

        coordinates.push_back(point);
    }

    fin.close();
    return coordinates;
}

double distance(std::pair<int, int> p1, std::pair<int, int> p2) {
    double result = std::sqrt(std::pow(p1.first - p2.first, 2) + std::pow(p1.second - p2.second, 2));
    return result;
}

double area(std::pair<int, int> p1, std::pair<int, int> p2, std::pair<int, int> p3) {
    double a = distance(p1, p2);
    double b = distance(p2, p3);
    double c = distance(p3, p1);
    double p = (a + b + c) / 2.0;
    
    double result = std::sqrt(p * (p - a) * (p - b) * (p - c));

    return result;
}

std::vector<std::pair<int, int>> findMaximumArea(std::vector<std::pair<int, int>> points) {    
    std::vector<std::pair<int, int>> result {{}, {}, {}};
    double maxArea = 0;
    double a = 0;
    for (int i{0}; i<points.size(); i++) {
        for (int j{i}; j<points.size(); j++) {
            for (int k{j}; k<points.size(); k++) {
                a = area(points[i], points[j], points[k]);
                if (a > maxArea) {
                    maxArea = a;
                    result[0] = points[i];
                    result[1] = points[j];
                    result[2] = points[k];
                }
            }
        }
    }
    return result;
}

void displayTriangle(std::vector<std::pair<int, int>> triangle) {
    std::cout << "Triangle with coordinatess: ";
    for (auto point: triangle) {
        std::cout << "[" << point.first << ", " << point.second << "]";
    }
    std::cout << std::endl;
}

void findAndDisplay(std::vector<std::pair<int, int>> points) {
    displayTriangle(findMaximumArea(points));
}


int main() {
    auto points1 = loadFile("pts1.dat");
    auto points2 = loadFile("pts2.dat");
    auto points3 = loadFile("pts3.dat");
    
    auto start {std::chrono::high_resolution_clock::now()};

    findAndDisplay(points1);
    findAndDisplay(points2);
    findAndDisplay(points3);
    
    auto stop {std::chrono::high_resolution_clock::now()};
    std::chrono::duration<double, std::milli> elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    start = std::chrono::high_resolution_clock::now();
    
    std::thread t1 {findAndDisplay, points1};
    std::thread t2 {findAndDisplay, points2};
    std::thread t3 {findAndDisplay, points3};
    
    t1.join();
    t2.join();
    t3.join();
    
    stop = std::chrono::high_resolution_clock::now();
    elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    return 0;
}
```

Вывод:

```
Triangle with coordinatess: [16, 97][97, -100][-99, -93]
Triangle with coordinatess: [-100, 21][70, -99][98, 99]
Triangle with coordinatess: [-82, 99][99, -93][-93, -100]
2102.28ms

Triangle with coordinatess: [-82, 99][99, -93][-93, -100]
Triangle with coordinatess: [16, 97][97, -100][-99, -93]
Triangle with coordinatess: [-100, 21][70, -99][98, 99]
772.817ms
```

Последовательность вывода может отличаться для многопоточного вызова.

Согласно заданию, каждый файл может быть обработан отдельно, т.е. нет зависимости между самими данными и результатами. Это позволяет вынести обработку каждого файла в отдельный поток. Для этого реализована функция `findAndDisplay`, которая принимает на вход вектор точек, находит решение и выводит результат. Решение основано на переборе всех комбинаций из трех точек и расчете площади треугольника по формуле Герона.

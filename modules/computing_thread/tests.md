# Вопросы по теме «Выносим вычисления в отдельный поток»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Какая ошибка, влияющая на скорость выполнения, допущена в коде ниже?

	```cpp
	#include <iostream>
	#include <thread>
	#include <chrono>

	using namespace std::chrono_literals;

	void sleep() {
	    std::this_thread::sleep_for(1000ms);
	}

	int main() {
	    std::thread t1 {sleep};
	    t1.join();
	    std::thread t2 {sleep};
	    t2.join();
	    return 0;
	}
	```

2.	Каким образом можно запустить блок кода в отдельном потоке?
3.	Можно ли запустить поток несколько раз?

# Паттерн Observer

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Реализация паттерна на С++. Примеры использования.

Паттерн `Observer` (наблюдатель) определяет отношение между объектами как один ко многим. Это значит, что при изменении состояния одного объекта все зависимые объекты будут уведомлены об этом и автоматически обновлены.
Например, одни и те же данные в пользовательском интерфейсе могут быть представлены в табличном и графическом виде, при этом при изменении данных в таблице необходимо автоматически поменять графическое представление. Шаблон "наблюдатель" позволяет задать связи между зависимыми объектами. Наблюдателей за объектом может быть несколько.

Реализацию данного паттерна можно разбить на два элемента: `Subject` — класс, который содержит набор наблюдателей и уведомляет их о каком-либо изменении, и `Observer` — класс, реализующий поведение при получении уведомления об изменениях `Subject`. Их классы-наследники реализуют конкретное поведение. В приведенном ниже примере представлен класс `Logger`. Данный класс отвечает за логирование. Он принимает какое-либо сообщение на вход и передает его всем обработчикам. Обработчики `FileHandler` и `CoutHandler` отвечают за обработку сообщений, первый записывает все сообщения в файл, второй отправляет в консоль. Количество наблюдателей может быть не ограничено ([код](../../projects/observer)):

```cpp
#include <iostream>
#include <fstream>
#include <map>
#include <list>


class Subject;

class Observer {
    public:
        ~Observer(){}
        virtual void update(std::string message) = 0;
    protected:
        Observer(){};
};

class Subject{
    private:
        std::map<std::string, Observer*> m_observers;
    public:
        ~Subject(){
            std::list<std::string> names {};

            for (const std::pair<std::string, Observer*>& observer: m_observers) {
                names.push_back(observer.first);
            }
            
            for (auto name: names) {
                std::cout << "Detach " << name << std::endl;
                detach(name);
            }
        }

        void attach(std::string name, Observer* observer) {
            std::cout<< "Attach " << name << std::endl;
            m_observers[name] = observer;
        }
        void detach(std::string name){
            if (m_observers.count(name)) {
                m_observers.erase(name);
            }
        }
        void notify(std::string message) {
            for (const std::pair<std::string, Observer*>& observer: m_observers) {
                std::cout << "Notify " << observer.first << std::endl;
                observer.second->update(message);
            }
        }
    protected:
        Subject(){};
};


class Logger: public Subject{
    public:
        Logger(){};
    
        void info(std::string message) {
            notify("INFO:" + message);
        }
        
        void error(std::string message) {
            notify("ERROR:" + message);
        }
};

class FileHandler: public Observer{
    private:
        std::string m_fileName;
        std::ofstream fout;

    public:
        FileHandler(std::string fileName): m_fileName{fileName}{
            fout.open(fileName, std::ios_base::app);
        }
        ~FileHandler(){
            fout.close();
        };
    
        void update(std::string message) {
            fout << message << std::endl;
        }
};

class CoutHandler: public Observer{
    public:
        void update(std::string message) {
            std::cout << message << std::endl;
        }
    
};


int main() {
    Logger logger {};
    FileHandler handler1 {"log1.txt"};
    FileHandler handler2 {"log2.txt"};
    CoutHandler cout {};

    logger.attach("file1", &handler1);
    logger.attach("file2", &handler2);
    logger.attach("console", &cout);

    logger.info("Test message");
    logger.error("Something wrong");

    return 0;
}
```

В представленной реализации все наблюдатели хранятся в структуре `std::map` по уникальному имени. Методы `attach` и `detach` подключают наблюдателей к классу. При этом класс `Logger` не отвечает за их время жизни. Данные наблюдатели в любой момент могут быть отключены/подключены.

При удалении объекта `Logger` происходит автоматическое отсоединение всех наблюдателей.

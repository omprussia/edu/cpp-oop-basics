# Задания по теме «Управление памятью: указатели и ссылки»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Написать функцию, которая принимает на вход массив и меняет в нем порядок элементов на обратный.
2.	Написать функцию, которая принимает на вход два массива одинаковой длины и меняет в них местами элементы с четными индексами.
3.	Написать функцию, которая принимает на вход указатель на массив и считает количество четных чисел.
4.	Написать функцию, которая меняет значения местами для двух переменных с использованием указателей.
5.	Написать программу для раскладывания одинаковых символов из строки, введенной пользователем, в один массив, где каждый элемент является массивом, содержащим все одинаковые символы из строки. Например, для строки "apple" ответом должен быть массив:

	```cpp
	[['a'], ['p', 'p'], ['l'], ['e']]
	```
	Программа должна позволять бесконечный ввод и вывод результата, пока пользователь не введет слово `exit`.

6.	Написать функцию, которая будет принимать массив и изменять его размер на заданное количество элементов. Если задано положительное число, то увеличивать массив, если отрицательное, то уменьшать массив.

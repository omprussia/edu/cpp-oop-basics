# Вопросы по теме «Управление памятью: указатели и ссылки»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Чему будет равен размер указателя на тип данных `char`?
2.	Какие значения будут выведены в примере ниже?

	```cpp
	int* array {new int[8] {5, 3, 7, 1, 9, 20, 1, 10}};
	int* parray {&array[4]};
	std::cout << *(parray - 1) << std::endl;
	std::cout << *(parray - 2) << std::endl;
	```

4.	К каким последствиям приведет данный код?

	```cpp
	for (;;) {
	    int* array = new int[100];
	}
	```

5.	Чему будет равна переменная `b` после выполнения данного кода?

	```cpp
	void function(int& value) {
	    value *= 4;
	}

	int main() {
	    int a = 10;
	    function(a);
	    std::cout << a << std::endl;
	    return 0;
	}
	```

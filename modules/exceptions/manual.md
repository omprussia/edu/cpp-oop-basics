# Исключительные ситуации

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Обработка исключительных ситуаций при помощи кодов ошибок и аппарата исключений. Создание своих исключений.

Любая программа подвержена ошибкам, особенно когда программами начинаются пользоваться не разработчики, а рядовые пользователи. Забота об ошибках — это ключевой момент при разработке надежного программного обеспечения. Необходимо научить программу работать с возможными ошибками и ненормальным поведением. Доступ к файлам, базам данных, сетевые запросы могут приводить к неожиданным ошибкам, так как программа не может контролировать то, что принадлежит не ей, например, сетевой провод, который соединяет базу данных с сетью, или USB-накопитель, который был отключен во время копирования файлов. Такие ошибки называются исключительными ситуациями — `exception`.

В качестве исключения может выступать любой объект, который сообщает о том, что произошла ошибка. Это может быть объект простого типа, но чаще применяются объекты классов, так как они могут содержать дополнительную информацию о происшествии.

Процесс работы с исключениями состоит из двух этапов: бросание (`throw`) исключения и передача исключения блоку обработки `catch`. Блок `try` находится перед `catch` и ловит исключение. Можно расставить "ловушки" на разные исключения и реализовать разный способ обработки.

Пример бросания исключения:

```cpp
int temperature {68};
if (temperature < 30) {
    std::cout << "Temperature is normal." << std::endl;
}
else {
    throw "Temperature is very high!";
}
```

В программе при температуре больше 29 градусов срабатывает вторая ветка условия, в которой бросается исключение в виде строки. Если запустить программу, то она завершится с ошибкой:

    terminate called after throwing an instance of 'char const*'
    Aborted (core dumped)

Программа "сломалась" из-за того, что в ней не предусмотрена обработка исключительной ситуации. Чтобы обработать ошибку, необходимо код обернуть в блок `try catch`:

```cpp
int temperature {68};
try{
    if (temperature < 30) {
        std::cout << "Temperature is normal." << std::endl;
    }
    else {
        throw "Temperature is very high!";
    }
}
catch (const char* message) {
    std::cout << "Something wrong: " << message << std::endl;
    return -1;
};
```

Блок `catch (const char* message)` отлавливает исключение только с указанным типом. Если добавить новое исключение с другим типом, то без соответствующего обработчика программа будет завершена с ошибкой:

```cpp
if (temperature < 30 && temperature > -20) {
    std::cout << "Temperature is normal." << std::endl;
}
else if (temperature < -20) {
    throw -1;
}
else {
    throw "Temperature is very high!";
}
```

Если температура ниже -20 градусов, то бросается исключение в виде числа `-1`, которое не совпадает с типом `const char* message`. Чтобы отловить и такого рода ошибку, нужен дополнительный блок `catch`:

```cpp
int temperature {-36};
try{
    if (temperature < 30 && temperature > -20) {
        std::cout << "Temperature is normal." << std::endl;
    }
    else if (temperature < -20) {
        throw -2;
    }
    else {
        throw "Temperature is very high!";
    }
}
catch (const char* message) {
    std::cout << "Something wrong: " << message << std::endl;
    return -1;
}
catch (int retVal) {
    std::cout << "Program is crashed!" << std::endl;
    return retVal;
};
```

Использование объектов в качестве исключений не отличается от использования других типов, но предоставляет возможность добавить любые информацию и методы, например, для удобного вывода ([код](../../projects/grades)):

```cpp
class Exception {
    private:
    std::string m_message {"Not defined"};
    public:
        Exception() {};
        Exception(std::string message): m_message{message}{
    };
    void display() {
        std::cout << "Exception occurred: " << m_message << std::endl;
    }
};

std::string gradeToText(int grade) {
    switch (grade) {
        case 1: return "poor";
        case 2: return "unsatisfactory";
        case 3: return "satisfactory";
        case 4: return "good";
        case 5: return "excellent";
        default: throw Exception("wrong grade");
    }
}

int main() {
    int grade;
    try{
        grade = 1;
        std::cout << gradeToText(grade) << std::endl;
        grade = 3;
        std::cout << gradeToText(grade) << std::endl;
        grade = 10;
        std::cout << gradeToText(grade) << std::endl;
    }
    catch (Exception e) {
        e.display();
    }
    return 0;
}
```

Вывод:

```
poor
satisfactory
Exception occurred: wrong grade
```

Исключения выбрасываются всегда на уровень выше до тех пор, пока не встретят блок `catch`. Если такого нет, то программа завершается с ошибкой:

```cpp
void function1() {
    throw -1;
}

void function2() {
    function1();
}

void function3() {
    function2();
}


int main() {
    try{
        function3();
    }
    catch (int retVal) {
        return retVal;
    }
    return 0;
}
```

Не все ошибки имеет смысл обрабатывать при помощи исключения. Исключения дороги в плане ресурсов и могут замедлять программу. Если есть возможность, ошибки необходимо обработать при помощи условий и возвращаемых значений.

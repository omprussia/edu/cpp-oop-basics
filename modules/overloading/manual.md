# Перегрузка операторов и методов

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Работа методов с разными типами данных. Перегрузка операторов присваивания, операторов доступа к элементам, арифметических операторов.

Язык C++ очень гибкий и мощный. Классы дают огромные возможности по настройке поведения объектов. Объекты могут вести себя как "числа" и поддерживать арифметические операции `+, -, *, /`, а могут обеспечивать доступ как к массиву при помощи `[]`. Методы класса могут принимать разные входные аргументы.

## Перегрузка функций

В языке С++ можно объявить несколько функций с одинаковым именем, но разными входными аргументами. У перегруженных функций может отличаться формат выходных значений:

```cpp
int triple(int n) {
return n * 3; 
}

double triple(double n) {
    return n * 3.;
}
```

В коде приведены два варианта функции `triple`. Первая принимает на вход целочисленное значение и возвращает утроенное значение того же типа. Вторая функция принимает на вход вещественное число и возвращает результат в вещественном виде:

```cpp
std::cout << triple(1.5) << std::endl;
std::cout << triple(10) << std::endl;
```

Вывод:

```
4.5
30
```

Компилятор сравнивает типы переданных аргументов и выбирает подходящую версию функции. Количество параметров также может варьироваться:

```cpp
int multiply(int a, int b) {
    return a * b;
}

int multiply(int a, int b, int c) {
    return a * b * c;
}
```

В таком случае функция подбирается согласно количеству аргументов и их типу.

### Перегрузка методов класса

Перегрузка для методов класса аналогична перегрузке для функций. Единственным методом, который нельзя перегрузить, является деструктор, так как у него нет параметров и всегда должна быть только одна реализация. Конструкторы могут быть перегружены, и при создании объекта будет вызван тот, который соответствует аргументам. В предыдущем разделе была применена перегрузка конструкторов:

```cpp
Item();
Item(std::string name, float weight);
```

Первый конструктор вызывался при пустой инициализации `Item sword {}`, второй при передаче параметров `Iten elixir {"Health potion", 0.2}`.

Пример перегрузки конструкторов и методов (для уменьшения количества кода все методы реализованы сразу, [код](../../projects/spells)):

```cpp
class Spell {
    private:
        std::string m_name;
        int m_cost {1};
        float m_castingTime {0.1};
        int m_minDamage {1};
        int m_maxDamage {1};
        
    public:
        Spell() = delete;

        Spell(std::string name) {
            m_name = name;
        };

        Spell(std::string name, int cost, float castingTime): m_name{name}, m_cost{cost}{
        };
        
        Spell(std::string name, int cost, float castingTime, int damage){
            m_name = name;
            m_cost = cost;
            m_castingTime = castingTime;
            m_minDamage = m_maxDamage = damage;
        }
        
        Spell(std::string name, int cost, float castingTime, int minDamage, int maxDamage){
            m_name = name;
            m_cost = cost;
            m_castingTime = castingTime;
            m_minDamage = minDamage;
            m_maxDamage = maxDamage;
        }

        void cast() {
            std::cout << "Casting a spell of '" << m_name << "' on yourself!" << std::endl;
        }

        void cast(std::string enemy) {
            std::cout << "Casting a spell of '" << m_name << "' on '" << enemy << "'!"<< std::endl;
        }

};


Spell empty {}; // приведет к ошибке
Spell light("Magic Light");
Spell heal("Healing Hands", 20, 0.5);
Spell fire("Fireball", 40, 0.2, 50);
Spell missiles("Arcane Missiles", 30, 0.2, 20, 40);

light.cast();
missiles.cast("goblin");
```

Вывод:

```
Casting a spell of 'Magic Light' on yourself!
Casting a spell of 'Arcane Missiles' on 'goblin'!
```

В примере приведено 4 конструктора и один метод `cast` с дополнительным вариантом вызова. Конструктор по умолчанию `Spell()` удален при помощи ключевого слова `delete`. Удаление конструктора по умолчанию запрещает создание объекта с пустым инициализатором.

Конструктор:

```cpp
Spell(std::string name, int cost, float castingTime): m_name{name}, m_cost{cost}{
};
```

Он имеет специальную форму инициализации переменных. Такая форма называется `список инициализаторов членов`. Инициализация членов класса происходит перед выполнением тела конструктора.

В классе объявлены две перегруженные функции: `cast()` и `cast(std::string)`. Первая вызывается, когда в метод ничего не передается, вторая — при передаче строки.

## Перегрузка операторов

Перегрузка операторов — это еще один механизм для расширения способностей объектов. Существует набор операторов, которые могут быть перегружены:

| +   | -   | *   | ⁄      | %     | ^        | &   | \|  | ~  |
|-----|-----|-----|--------|-------|----------|-----|-----|----|
| !   | =   | <   | >      | +=    | -=       | *=  | ⁄=  | %= |
| ^=  | &=  | \|= | <<     | >>    | <<=      | >>= | ==  | != |
| <=  | >=  | &&  | \|\|   | ++    | --       | ,   | ->* | -> |
| ( ) | [ ] | new | delete | new[] | delete[] |     |     |    |

Чтобы перегрузить оператор, необходимо в классе использовать ключевое слово `operator` вместе с символом операции:

```
operator+
operator-
operator[]
```

Пример реализации операторов `[]`, `+` и `*` ([код](../../projects/color)):

```cpp
class Color {

public:
    unsigned short m_red {0};
    unsigned short m_green {0};
    unsigned short m_blue {0};

    Color(){};
    Color(unsigned short red, unsigned short green, unsigned short blue ) : m_red {red}, m_green {green}, m_blue {blue} {}
    
    Color operator+(Color other) {
        Color newColor {};
        newColor.m_red = (*this)[0] + other[0];
        newColor.m_green = (*this)[1] + other[1];
        newColor.m_blue = (*this)[2] + other[2];
        return newColor;
    }
        
    Color operator/(unsigned short n) {
        Color newColor {};
        newColor.m_red = (*this)[0] / n;
        newColor.m_green = (*this)[1] / n;
        newColor.m_blue = (*this)[2] / n;
        return newColor;
    }

    unsigned short operator[](unsigned short i) {
        switch (i) {
            case 0:
                return m_red;
            case 1:
                return m_green;
            case 2:
                return m_blue;
            default:
                return 0;
        }
    }
};


int main() {
    Color color1 {100, 100, 100};
    Color color2 {10, 20, 30};
    Color mix {color1 + color2};
    std::cout << mix[0] << " " << mix[1] << " " << mix[2] << std::endl;
    
    Color divColor {color1 / 2};
    std::cout << divColor[0] << " " << divColor[1] << " " << divColor[2] << std::endl;
    
    return 0;
}
```

Ограничения на перегрузку:

*	один из операндов должен быть типом, определенным разработчиком;
*	нельзя придумывать свои операторы;
*	перегруженные методы не будут работать в обратном порядке `Color multColor {2 * color1};`.

Чтобы работала перегрузка в обратном порядке, используется специальная форма перегруженных функций с модификатором `friend`:

```cpp
friend Color operator*(int n, const Color &color) {
    Color newColor {};
    newColor.m_red = color.m_red * n;
    newColor.m_green = color.m_green * n;
    newColor.m_blue = color.m_blue * n;
    return newColor;
}
```

Данный метод сработает, когда в выражении слева от оператора `/` будет стоять целое число.

Перегрузка оператора `()` придаст классу поведение функции (../../projects/polynomial):

```cpp
class Polynomial {
    private:
        double m_a {0.0};
        double m_b {0.0};
        double m_c {0.0};

    public:
        Polynomial(){};
        Polynomial(double a, double b, double c): m_a{a}, m_b{b}, m_c{c} {
        };
        double operator()(double x) {
            return m_a * x * x + m_b * x + m_c;
        }
};

int main() {
    Polynomial poly {2.6, 1.5, 3.2};

    std::cout << poly(2.5) << std::endl;
    std::cout << poly(-1.0) << std::endl;
    std::cout << poly(3.3) << std::endl;
    
    return 0;
}
```

Перегрузка оператора `()` удобна для частичной инициализации функции. Это позволяет получить решения с частично одинаковыми аргументами и не передавать их в качестве входных аргументов.

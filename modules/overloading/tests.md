# Вопросы по теме «Перегрузка операторов и методов»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Какой оператор необходимо перегрузить, чтобы класс вел себя как функция?
2.	Какой конструктор будет вызван в данном примере?

	```cpp
	class Object {
	    public:
	        Object(){}
	        Object(int x, float y){
	            std::cout << "1" << std::endl;
	        }
	        Object(float x, int y){
	            std::cout << "2" << std::endl;
	        }
	        Object(float x, float y){
	            std::cout << "3" << std::endl;
	        }
	};

	Object obj {1.5, 1.5};
	```

3.	Сколько раз может быть перегружен метод?

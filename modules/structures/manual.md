# Структуры данных

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Объединение данных в собственный тип данных. Доступ к полям.

Данные, используемые в программах, разнообразны и имеют разную природу и структуру. Часто бывает так, что некоторые сущности могут описываться целым набором параметров, которые логически связаны. Например, город имеет координаты, долготу и широту, название, площадь, население и т. д. Пример того, каким образом можно представить в программе несколько городов ([код](../../projects/city_example)):

```cpp
void printCity(char* name, float latitude, float longitude, float area, int population) {
    std::cout << name << " " << latitude << " " << longitude << " ";
    std::cout << area << " " << population << std::endl;
}

int main() {
    char name[5][12] {"Moscow", "Irkutsk", "Tomsk", "Tyumen", "Anadyr"};
    float latitude[5] {55.76, 52.28, 56.5, 57.15, 64.73};
    float longitude[5] {37.62, 104.28, 84.97, 65.53, 177.52};
    float area[5] {2561.5, 277, 294.6, 698, 20};
    int population[5] {13010112, 587891, 524669, 581907, 13045};

    for (int i {}; i<5; i++){
        printCity(name[i], latitude[i], longitude[i], area[i], population[i]);
    }

    return 0;
}
```

Объявление вида `char name[5][12]` представляет собой массив массивов размером 5 для хранения имен, и каждое имя представлено в виде массива символов с максимальным размером 12.

В приведенном примере для хранения всех данных для городов требуется пять массивов. Функции, которые работают с городами, будут также требовать передачи всех пяти массивов для доступа ко всей информации. В языке С++ существует специальный способ для хранения связанных значений — структуры. Структура — пользовательский тип данных, который может содержать несколько значений разных типов и предоставлять к ним доступ. Для определения такого типа необходимо описать структуру хранения данных в виде типизированных именованных полей:

```cpp
struct city {
    char name[12];
    float latitude;
    float longitude;
    float area;
    int population;
};
```

После этого можно создать переменную с таким типом и указатель на нее:

```cpp
city moscow {"Moscow", 55.76, 37.62, 2561.5, 13010112};
city* pmoscow {&moscow};
```

Чтобы получить доступ к отдельным полям, используется оператор доступа к членам `.` или `->`. Оператор "стрелка" используется при доступе через указатель:

```cpp
std::cout << moscow.name << " " << pmoscow->population << std::endl;
```

Оператор `->` равнозначен обращению через оператор непрямого обращения `*`:

```cpp
std::cout << (*pmoscow).latitude << std::endl;
```

Так как структура описывает тип, то его можно использовать и при объявлении функции, и при передаче аргументов, и при создании массивов:

```cpp
void printCity(city c) {
    std::cout << c.name << " " << c.latitude << " " << c.longitude << " ";
    std::cout << c.area << " " << c.population << std::endl;
}
```

Создание и заполнение массива городов:

```cpp
city cityArray[5] {{"Moscow", 55.76, 37.62, 2561.6, 13010112},
                    {"Irkutsk", 52.28, 104.28, 277., 587891},
                    {"Tomsk", 56.5, 84.97, 294.6, 524669},
                    {"Tyumen", 57.15, 65.53, 698, 581907},
                    {"Anadyr", 64.73, 65.53, 20, 13045}};
for (int i {}; i<5; i++){
    printCity(cityArray[i]);
}
```

Для полей в структуре данных можно установить значения по умолчанию:

```cpp
struct city {
    char name[12] {"Unknown"};
    float latitude {-1.};
    float longitude {-1.};
    float area {-1.};
    int population {-1};
};
```

При создании структуры с пустой инициализацией все поля будут установлены со значением по умолчанию:

```cpp
city defaultCity {};

std::cout << defaultCity.name << " " << defaultCity.area << std::endl;
```

Вывод:

```
Unknown -1
```

Структуры могут использоваться в любом месте программы, включая другие структуры. Например, можно объединить координаты в отдельную структуру:

```cpp
struct location {
    float latitude {-1.};
    float longitude {-1.};
};

struct city {
    char name[12] {"Unknown"};
    location coordinates {};
    float area {-1.};
    int population {-1};
};
```

В таком случае инициализацию и функцию необходимо изменить ([код](../../projects/city_struct_example)):

```cpp
void printCity(city c) {
    std::cout << c.name << " " << c.coordinates.latitude << " " << c.coordinates.longitude << " ";
    std::cout << c.area << " " << c.population << std::endl;
}

city cityArray[5] {{"Moscow", {55.76, 37.62}, 2561.6, 13010112},
                    {"Irkutsk", {52.28, 104.28}, 277., 587891},
                    {"Tomsk", {56.5, 84.97}, 294.6, 524669},
                    {"Tyumen", {57.15, 65.53}, 698, 581907},
                    {"Anadyr", {64.73, 65.53}, 20, 13045}};
```

Доступ к полю `latitude` через указатель будет выглядеть следующим образом:

```cpp
std::cout << (*pmoscow).coordinates.latitude << std::endl;
```

# Вопросы по теме «Структуры данных»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Для описания каких видов данных из реального мира можно использовать структуры?
2.	Каким образом можно получить доступ к полю данных `value` для следующей структуры?

	```cpp
	struct data {
	    float* value = new float {1.5};
	};

	int main() {
	    data a;
	    std::cout << *a.value << std::endl; //1
	    std::cout << a->value << std::endl; //2
	    std::cout << (*a)->value << std::endl; //3
	}
	```

3.	А для той же структуры данных, но с другим объявлением?

	```cpp
	data* a = new data;

	std::cout << *a->value << std::endl; //1
	std::cout << *(a->value) << std::endl; //2
	std::cout << *(*a).value << std::endl; //3
	```

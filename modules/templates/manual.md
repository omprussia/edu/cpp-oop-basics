# Шаблоны

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Шаблонные функции и классы

Перегрузка функций помогает сделать несколько вариантов с разными аргументами:

```cpp
void display(double value) {
    std::cout << "Value = " << value << std::endl;
}

void display(int value) {
    std::cout << "Value = " << value << std::endl;
}

void display(char value) {
    std::cout << "Value = " << value << std::endl;
}
```

Единственная разница между этими функциями заключается в типе данных. Такое решение выглядит избыточным. Чтобы уменьшить количество кода, можно оформить шаблон. Шаблоны применяются как для функций, так и для классов.

## Шаблонные функции

Шаблонная функция позволяет объявить целую серию функций, которые могут работать сразу с большим набором типов без повторения кода. Компилятор на основе описанного шаблона сгенерирует несколько функций, подходящих под определенный тип.

Описание шаблонной функции начинается с ключевого слова `template`:

```cpp
template <typename T>
void display(T value) {
    std::cout << "Value = " << value << std::endl;
}
```

В угловых скобках при помощи ключевого слова `typename` указывается, что `T` является типом. Везде, где будет указано `T`, произойдет замена на выбранный тип данных. Для создания определенной версии функции необходимо указать тип данных:

```cpp
display<int>(10);
display<double>(1.5);
display<char>('A');
```

Тип данных указывается в угловых скобках. Это необязательно, так как компилятор способен автоматически вывести тип данных входного аргумента:

```cpp
display(true);
```

Шаблонных типов может быть несколько:

```cpp
template <typename T, typename K>
void displayTwoVariables(T a, K b) {
    std::cout << "a = " << a << " b = " << b << std::endl;
}

displayTwoVariables(12, 1.5);
displayTwoVariables("X", "Y");
```

При вызове функций с двумя шаблонными типами в угловых скобках можно указать оба типа:

```cpp
displayTwoVariables<int, double>(12, 1.5);
displayTwoVariables<char, char>("X", "Y");
```

Шаблонный тип используется не только для указания типов входных аргументов и возвращаемого значения, но и для локальных переменных:

```cpp
template <typename T>
T triple(T value){
    T result {value * 3};
    return result;
}
```

При создании шаблона можно указать вместо типа данных типизированное значение:

```cpp
template <typename T, int n>
void function(T value) {
    for(int i {0}; i < n; i++) {
        std::cout << value << std::endl;
    }
}
```

Данное значение задается при спецификации функции и вычисляется статически в момент компиляции программы:

```cpp
function<int, 10>(2);
function<float, 5>(1.5);
```

## Шаблонные классы

Механизм, аналогичный созданию шаблонных функций, существует и для классов. Для классов также используется ключевое слово `template`:

```cpp
template <typename T>
class Location {
    private:
        T m_latitude {};
        T m_longitude {};

    public:
        Location(T latitude, T longitude): m_latitude{latitude}, m_longitude{longitude} {}
        T getLatitude() {
            return m_latitude;
        };
        T getLongitude() {
            return m_longitude;
        };

};
```

Чтобы использовать шаблонный класс, необходимо указать тип:

```cpp
Location<int> location {10, 12};
Location<float> fLocation {1.5, 0.5};

std::cout << location.getLatitude() << std::endl;
std::cout << fLocation.getLongitude() << std::endl;
```

# Вопросы по теме «Шаблоны»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Для чего нужны шаблоны?
2.	Для чего используется ключевое слово `typename`?
3.	Правильно ли реализована шаблонная функция ниже?

	```cpp
	template <typename T, typename V>
	double function(T a, V b) {
	    return 2 + (a / b);
	}

	std::cout << function<int, int>(10, 2.3) << std::endl;
	```

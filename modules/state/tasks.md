# Задания по теме «Паттерн State»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Расширить код класса `AirConditioner` так, чтобы его можно было переводить в состояние `OffState`.
2.	Добавить состояние `BrokenState`. В данное состояние `AirConditioner` переходит с некоторой вероятностью, например, с шансом 10% при включении. Для реализации случайности использовать код:

	```cpp
	#include <iostream>
	#include <cstdlib>
	#include <ctime>

	double probability() {
	    return static_cast<double>(std::rand()) / RAND_MAX;
	}

	int main() {
	    std::srand(std::time(nullptr));
	    for (int i {0}; i < 5; i++){
	        std::cout << probability() << std::endl;
	    }
	    return 0;
	}
	```

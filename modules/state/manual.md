# Паттерн State

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

> Реализация паттерна на С++. Примеры использования.

Паттерн "состояние" (`State`) используется для описания объекта, который меняет поведение в зависимости от своего состояния. Например, таким объектом может быть файл. Состояние файла описывается флагами и может быть таким: не открыт, открыт, ошибка, закрыт. В зависимости от текущего состояния доступны разные методы.

Данный паттерн применяется в случаях, когда объект в момент выполнения программы должен менять свое поведение, или действие, которое должен совершить объект, имеет многоступенчатую инициализацию ([код](../../projects/state)):

```cpp
class AirConditioner;

class State {
    protected:
        AirConditioner* m_heater;

    public:
        virtual ~State() {};

        void setAirConditioner(AirConditioner* heater) {
            m_heater = heater;
        }

    virtual void heat() = 0;
    virtual void cool() = 0;
    virtual void on() = 0;

};

class AirConditioner {
    private:
        State* m_state;

    public:
        AirConditioner(State* state): m_state{nullptr} {
            transitionTo(state);
        }

        ~AirConditioner() {
            delete m_state;
        }
    
        void transitionTo(State* state) {
            if (m_state) {
                delete m_state;
            }
            m_state = state;
            m_state->setAirConditioner(this);
        }
        
        void heat() {
            m_state->heat();
        }
        
        void cool() {
            m_state->cool();
        }

        void on() {
            m_state->on();
        }

};

class OnState: public State {
    public:
        void heat() {
            std::cout << "Heating..." << std::endl;
        }
        void cool() {
            std::cout << "Cooling..." << std::endl;
        }
        void on() {
            std::cout << "Already is on" << std::endl;
        }
};

class OffState: public State {
    public:
        void heat() {
            std::cout << "Can't heat. State is off" << std::endl;
        }
        void cool() {
            std::cout << "Can't cool. State is off" << std::endl;
        }
        void on() {
            std::cout << "Power on" << std::endl;
            m_heater->transitionTo(new OnState);
        }
};


int main() {
    AirConditioner heater {new OffState};
    heater.cool();
    heater.heat();
    heater.on();
    heater.on();
    heater.cool();
    heater.heat();
    return 0;
}
```

Класс `State` является абстрактным классом, описывающим всю работу, которая может быть выполнена определенным классом. Классы-наследники описывают конкретное состояние и определяют способ выполнения работы при нем. В классе `Heater` объявлен метод `transitionTo`, отвечающий за переход из одного состояния в другое и очистку памяти. Данный класс "не знает" конкретную реализацию выполняемой работы, а запрашивает у текущего состояния. Это позволяет предотвратить выполнение какой-либо работы, если текущее состояние не подходит для ее выполнения.

# Задания по теме «Очереди и синхронизация потоков»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

1.	Реализовать специальный класс, который, используя очередь, позволяет записывать значения в файл из разных потоков.
2.	Реализовать программу, которая, используя многопоточность, для заданного массива увеличит все элементы на порядок. При старте программы должны задаваться размер массива и количество потоков. Каждый поток обрабатывает свою часть массива. Необходимо также распределить количество данных между потоками так, чтобы они все выполнили примерно одинаковую часть работы.

	Например, задан массив `{1,2,3,4,5,6,7,8}` и `2` потока. Тогда каждый поток возьмет по половине массива, первый возьмет `{1, 2, 3, 4}`, второй — `{5, 6, 7, 8}`. После этого каждый поток увеличит на порядок все переданные значения. На выходе должен получиться измененный массив: `{10, 20, 30, 40, 50, 60, 70, 80}`.
	Если количество потоков будет равно `3`, то задачи должны быть разделены следующим образом: `{1, 2, 3}`, `{4, 5, 6}` и `{7, 8}`.

	После решения задачи нужно ответить на вопрос: чему будет равно время выполнения задачи при неравномерном распределении задач?

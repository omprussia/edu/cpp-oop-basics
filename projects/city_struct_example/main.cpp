// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

struct location {
    float latitude {-1.};
    float longitude {-1.};
};

struct city {
    char name[12] {"Unknown"};
    location coordinates {};
    float area {-1.};
    int population {-1};
};

void printCity(city c) {
    std::cout << c.name << " " << c.coordinates.latitude << " " << c.coordinates.longitude << " ";
    std::cout << c.area << " " << c.population << std::endl;
}

int main() {
    city cityArray[5] {{"Moscow", {55.76, 37.62}, 2561.6, 13010112},
                       {"Irkutsk", {52.28, 104.28}, 277., 587891},
                       {"Tomsk", {56.5, 84.97}, 294.6, 524669},
                       {"Tyumen", {57.15, 65.53}, 698, 581907},
                       {"Anadyr", {64.73, 65.53}, 20, 13045}};

    for (int i {}; i<5; i++){
        printCity(cityArray[i]);
    }

    return 0;
}

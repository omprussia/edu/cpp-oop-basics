// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <fstream>
#include <map>
#include <list>


class Subject;

class Observer {
    public:
        ~Observer(){}
        virtual void update(std::string message) = 0;
    protected:
        Observer(){};
};

class Subject{
    private:
        std::map<std::string, Observer*> m_observers;
    public:
        ~Subject(){
            std::list<std::string> names {};

            for (const std::pair<std::string, Observer*>& observer: m_observers) {
                names.push_back(observer.first);
            }
            
            for (auto name: names) {
                std::cout << "Detach " << name << std::endl;
                detach(name);
            }
        }

        void attach(std::string name, Observer* observer) {
            std::cout<< "Attach " << name << std::endl;
            m_observers[name] = observer;
        }
        void detach(std::string name){
            if (m_observers.count(name)) {
                m_observers.erase(name);
            }
        }
        void notify(std::string message) {
            for (const std::pair<std::string, Observer*>& observer: m_observers) {
                std::cout << "Notify " << observer.first << std::endl;
                observer.second->update(message);
            }
        }
    protected:
        Subject(){};
};


class Logger: public Subject{
    public:
        Logger(){};
    
        void info(std::string message) {
            notify("INFO:" + message);
        }
        
        void error(std::string message) {
            notify("ERROR:" + message);
        }
};

class FileHandler: public Observer{
    private:
        std::string m_fileName;
        std::ofstream fout;

    public:
        FileHandler(std::string fileName): m_fileName{fileName}{
            fout.open(fileName, std::ios_base::app);
        }
        ~FileHandler(){
            fout.close();
        };
    
        void update(std::string message) {
            fout << message << std::endl;
        }
};

class CoutHandler: public Observer{
    public:
        void update(std::string message) {
            std::cout << message << std::endl;
        }
    
};


int main() {
    Logger logger {};
    FileHandler handler1 {"log1.txt"};
    FileHandler handler2 {"log2.txt"};
    CoutHandler cout {};

    logger.attach("file1", &handler1);
    logger.attach("file2", &handler2);
    logger.attach("console", &cout);

    logger.info("Test message");
    logger.error("Something wrong");

    return 0;
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

void triple(int &a, double &b) {
	a *= 3;
	b *= 1.5;
}

int main() {
	int a = 10;
	double b = 10.0;
	triple(a, b);
    std::cout << a << " " << b << std::endl;
    return 0;
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

void sleep() {
        std::this_thread::sleep_for(1s);
    }

int main() {
    const int nthreads {4};
    std::thread threads[nthreads];

    auto start {std::chrono::high_resolution_clock::now()};
    
    for (int i{0}; i < nthreads; i++) {
        sleep();
    }

    auto stop {std::chrono::high_resolution_clock::now()};

    std::chrono::duration<double, std::milli> elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";


    start = std::chrono::high_resolution_clock::now();

    for (int i{0}; i < nthreads; i++) {
        threads[i] = std::thread {sleep};
    }
    
    for (int i{0}; i < nthreads; i++) {
        threads[i].join();
    }
    
    stop = std::chrono::high_resolution_clock::now();
    elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    return 0;
}

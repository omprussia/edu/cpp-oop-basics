// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef LOGGER_H_
#define LOGGER_H_

#include <fstream>


class Logger {

    public:
        ~Logger(){
            m_out.close();
        }
        static Logger* getInstance();
        void info(std::string message);
        void debug(std::string message);

    protected:
        Logger(){
            m_out.open(m_fileName, std::ios_base::app);
        };

    private:
    	const char* m_fileName {"log.txt"};
        static Logger* _instance;
        std::ofstream m_out;
    
};


#endif

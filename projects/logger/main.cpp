// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include "logger.h"

int prod(int* array, int size) {
    int result = 1;
    
    Logger::getInstance()->debug("Called function prod");
    
    for (int i{}; i<size; i++) {
        result *= array[i];
    }
    
    return result;
}

int sum(int* array, int size) {
    int result = 0;
    
    Logger::getInstance()->debug("Called function sum");
    
    for (int i{}; i<size; i++) {
        result += array[i];
    }
    
    return result;
}

int main() {    
    Logger::getInstance()->info("Programm started!");
    int array[] {1, 2, 3, 4, 5};
    
    std::cout << prod(array, 5) << std::endl;
    std::cout << sum(array, 5) << std::endl;

    Logger::getInstance()->info("Programm done!");
    return 0;
}

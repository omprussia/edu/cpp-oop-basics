// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "logger.h"

Logger* Logger::_instance = nullptr;

Logger* Logger::getInstance(){
    if (_instance == nullptr) {
        _instance = new Logger;
    }
    return _instance;
}

void Logger::info(std::string message) {
    m_out << "INFO: " << message << std::endl;
}

void Logger::debug(std::string message) {
    m_out << "DEBUG: " << message << std::endl;
}


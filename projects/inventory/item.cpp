// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "item.h"

Item::Item() {
}

Item::Item(std::string name, float weight) {
    this->name = name;
    this->weight = weight;
}

void Item::display(bool nameOnly) {
    std::cout << "Name: " << name;
    if (!nameOnly) {
        std::cout << ", weight: " << weight << ", id: " << id;
    }
    std::cout << std::endl;
}

int Item::getId() {
    return id;
}

float Item::getWeight() {
    return weight;
}

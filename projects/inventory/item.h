// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef ITEM_H_
#define ITEM_H_

#include <iostream>

class Item {
    private:
        unsigned short id {1};
        std::string name {"default"};
        float weight {0.0};

    public:
        Item();
        Item(std::string name, float weight);
        
        void display(bool nameOnly);
        int getId();
        float getWeight();

};

#endif

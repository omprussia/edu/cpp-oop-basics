// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef INVENTORY_H_
#define INVENTORY_H_

#include "item.h"

class Inventory {
    private:
        Item** storage {nullptr};
        int size {0};
        float maxWeight {0};

    public:
        Inventory(int size, float maxWeight);
        ~Inventory();
        bool addItem(std::string name, float weight);
        void delItem(int index);
        float getCurrentWeight();
};

#endif

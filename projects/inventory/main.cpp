// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include "inventory.h"
#include "item.h"


int main() {
    Inventory inventory {3, 5};

    std::cout << inventory.getCurrentWeight() << std::endl;
    
    std::cout << std::boolalpha << inventory.addItem("Health potion", 0.5) << std::endl;
    std::cout << std::boolalpha << inventory.addItem("Sword", 3) << std::endl;
    std::cout << std::boolalpha << inventory.addItem("Helmet", 3) << std::endl;
    std::cout << std::boolalpha << inventory.addItem("Mana potion", 0.5) << std::endl;
    
    inventory.delItem(1);
    std::cout << std::boolalpha << inventory.addItem("Helmet", 3) << std::endl;

    std::cout << inventory.getCurrentWeight() << std::endl;

    return 0;
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "inventory.h"

Inventory::Inventory(int size, float maxWeight) {
    this->size = size;
    this->storage = new Item*[this->size];

    for (int i{}; i < size; i++) {
        this->storage[i] = nullptr;
    }

    this->maxWeight = maxWeight;
}

Inventory::~Inventory() {
    std::cout << "Free memory" << std::endl;
    for (int i{}; i < size; i++) {
        if (storage[i] != nullptr) {
            delete storage[i];
            storage[i] = nullptr;
        }
    }
    delete[] storage;
    storage = nullptr;
}

bool Inventory::addItem(std::string name, float weight){
    float totalWeight = getCurrentWeight();
    
    for (int i{}; i < size; i++) {
        if ((storage[i] == nullptr) && ((totalWeight + weight) < maxWeight)) {
            storage[i] = new Item(name, weight);
            return true;
        }
    }
    return false;
}

void Inventory::delItem(int index) {
    if ((index >= 0) && (index < size)) {
        if (storage[index] != nullptr) {
            delete storage[index];
            storage[index] = nullptr;
        }
    }
}

float Inventory::getCurrentWeight() {
    float totalWeight = 0;
    for (int i{}; i < size; i++) {
        if (storage[i] != nullptr) {
            totalWeight += storage[i]->getWeight();
        }
    }
    return totalWeight;
}

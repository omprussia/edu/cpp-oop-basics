// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

class Enemy {
    private:
        std::string m_name = {};

    protected:
        int m_hp = 0;
    
    public:
        Enemy(){};
        Enemy(std::string name, int hp): m_name {name}, m_hp {hp}{
        }

        void display() {
            std::cout << "Enemy: " << m_name << " with hp = " << m_hp << std::endl;
        }
        
        void sleep() {
            std::cout << "Enemy: sleep"  << std::endl;
        }
};

class Mage: public Enemy {
    protected:
        int m_mp = 0;

    public:
        Mage(std::string name, int hp, int mp) : Enemy(name, hp), m_mp{mp} {
        }
};

int main() {
    Mage mage {"Koschei the Deathless", 20, 50};
    mage.display();
    
    return 0;
}


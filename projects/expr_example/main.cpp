// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <cmath>

int main() {
    float a; // выражение объявление вещественной переменной 'a'        
    a = 5.; // выражение присваивания значения переменной 'a'
    
    float h = a * std::sqrt(3) / 2;
    float s = 0.5 * a * h;
    std::cout << "Area = " << s << std::endl;
    return 0;
}

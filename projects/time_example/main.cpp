// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

int main() {
    int totalSeconds {4555};
    int seconds {totalSeconds % 60};
    int minutes {totalSeconds / 60};
    int hours {minutes / 60};
    
    std::cout << "Elapsed time: " << hours << ":" << minutes % 60 << ":" << seconds << std::endl;
    return 0;
}

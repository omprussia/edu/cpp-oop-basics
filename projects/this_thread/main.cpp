// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main() {
    std::cout << "Main thread id: " << std::this_thread::get_id() << std::endl;
    std::cout << "Sleep for 1 second" << std::endl;
    std::this_thread::sleep_for(1000ms);
    std::cout << "Done" << std::endl;
    return 0;
}

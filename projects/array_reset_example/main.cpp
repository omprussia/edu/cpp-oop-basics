// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

void resetFirstElement(int array[]) {
    array[0] = 0;
}

int main() {
    int a[] {10, 20, 30, 40};
    resetFirstElement(a);
    std::cout << a[0] << std::endl;
    return 0;
}

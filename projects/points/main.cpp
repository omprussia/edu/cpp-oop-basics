// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <thread>
#include <chrono>
#include <vector>
#include <cmath>

using namespace std::chrono_literals;


std::vector<std::pair<int, int>> loadFile(std::string fileName) {
    std::ifstream fin;
    fin.open(fileName);
    if (!fin.is_open()) {
        throw "Can't load file";
    }

    std::vector<std::pair<int, int>> coordinates {};

    std::string line {};
    std::pair<int, int> point;
    std::istringstream sline {};

    while (fin) {
        std::getline(fin, line);
        sline.clear();
        sline.str(line);
        sline >> point.first;
        sline >> point.second;

        coordinates.push_back(point);
    }

    fin.close();
    return coordinates;
}

double distance(std::pair<int, int> p1, std::pair<int, int> p2) {
    double result = std::sqrt(std::pow(p1.first - p2.first, 2) + std::pow(p1.second - p2.second, 2));
    return result;
}

double area(std::pair<int, int> p1, std::pair<int, int> p2, std::pair<int, int> p3) {
    double a = distance(p1, p2);
    double b = distance(p2, p3);
    double c = distance(p3, p1);
    double p = (a + b + c) / 2.0;
    
    double result = std::sqrt(p * (p - a) * (p - b) * (p - c));

    return result;
}

std::vector<std::pair<int, int>> findMaximumArea(std::vector<std::pair<int, int>> points) {    
    std::vector<std::pair<int, int>> result {{}, {}, {}};
    double maxArea = 0;
    double a = 0;
    for (int i{0}; i<points.size(); i++) {
        for (int j{i}; j<points.size(); j++) {
            for (int k{j}; k<points.size(); k++) {
                a = area(points[i], points[j], points[k]);
                if (a > maxArea) {
                    maxArea = a;
                    result[0] = points[i];
                    result[1] = points[j];
                    result[2] = points[k];
                }
            }
        }
    }
    return result;
}

void displayTriangle(std::vector<std::pair<int, int>> triangle) {
    std::cout << "Triangle with coordinatess: ";
    for (auto point: triangle) {
        std::cout << "[" << point.first << ", " << point.second << "]";
    }
    std::cout << std::endl;
}

void findAndDisplay(std::vector<std::pair<int, int>> points) {
    displayTriangle(findMaximumArea(points));
}


int main() {
    auto points1 = loadFile("pts1.dat");
    auto points2 = loadFile("pts2.dat");
    auto points3 = loadFile("pts3.dat");
    
    auto start {std::chrono::high_resolution_clock::now()};

    findAndDisplay(points1);
    findAndDisplay(points2);
    findAndDisplay(points3);
    
    auto stop {std::chrono::high_resolution_clock::now()};
    std::chrono::duration<double, std::milli> elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    start = std::chrono::high_resolution_clock::now();
    
    std::thread t1 {findAndDisplay, points1};
    std::thread t2 {findAndDisplay, points2};
    std::thread t3 {findAndDisplay, points3};
    
    t1.join();
    t2.join();
    t3.join();
    
    stop = std::chrono::high_resolution_clock::now();
    elapsed = stop - start;
    std::cout << elapsed.count() << "ms\n";

    return 0;
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>


class Exception {
    private:
    std::string m_message {"Not defined"};
    public:
        Exception() {};
        Exception(std::string message): m_message{message}{
    };
    void display() {
        std::cout << "Exception occurred: " << m_message << std::endl;
    }
};

std::string gradeToText(int grade) {
    switch (grade) {
        case 1: return "poor";
        case 2: return "unsatisfactory";
        case 3: return "satisfactory";
        case 4: return "good";
        case 5: return "excellent";
        default: throw Exception("wrong grade");
    }
}

int main() {
    int grade;
    try{
        grade = 1;
        std::cout << gradeToText(grade) << std::endl;
        grade = 3;
        std::cout << gradeToText(grade) << std::endl;
        grade = 10;
        std::cout << gradeToText(grade) << std::endl;
    }
    catch (Exception e) {
        e.display();
    }
    return 0;
}


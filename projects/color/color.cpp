// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "color.h"

Color::Color(){
};

Color::Color(unsigned short red, unsigned short green, unsigned short blue ) : m_red {red}, m_green {green}, m_blue {blue} {
}
    
Color Color::operator+(Color other) {
    Color newColor {};
    newColor.m_red = (*this)[0] + other[0];
    newColor.m_green = (*this)[1] + other[1];
    newColor.m_blue = (*this)[2] + other[2];
    return newColor;
}
        
Color Color::operator/(unsigned short n) {
    Color newColor {};
    newColor.m_red = (*this)[0] / n;
    newColor.m_green = (*this)[1] / n;
    newColor.m_blue = (*this)[2] / n;
    return newColor;
}

unsigned short Color::operator[](unsigned short i) {
    switch (i) {
        case 0:
            return m_red;
        case 1:
            return m_green;
        case 2:
            return m_blue;
        default:
            return 0;
    }
}


// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include "color.h"

int main() {
    Color color1 {100, 100, 100};
    Color color2 {10, 20, 30};
    Color mix {color1 + color2};
    std::cout << mix[0] << " " << mix[1] << " " << mix[2] << std::endl;
    
    Color divColor {color1 / 2};
    std::cout << divColor[0] << " " << divColor[1] << " " << divColor[2] << std::endl;

	Color multColor {2 * color1};
    std::cout << multColor[0] << " " << multColor[1] << " " << multColor[2] << std::endl;
    
    return 0;
}

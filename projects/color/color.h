// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef COLOR_H_
#define COLOR_H_

#include <iostream>

class Color {

    public:
        unsigned short m_red {0};
        unsigned short m_green {0};
        unsigned short m_blue {0};

        Color();
        Color(unsigned short red, unsigned short green, unsigned short blue );
        Color operator+(Color other);
        Color operator/(unsigned short n);
        unsigned short operator[](unsigned short i);

        friend Color operator*(int n, const Color &color) {
			Color newColor {};
			newColor.m_red = color.m_red * n;
			newColor.m_green = color.m_green * n;
			newColor.m_blue = color.m_blue * n;
			return newColor;
		}
};


#endif

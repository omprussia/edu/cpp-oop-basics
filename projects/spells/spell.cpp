// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "spell.h"

Spell::Spell(std::string name) {
    m_name = name;
};

Spell::Spell(std::string name, int cost, float castingTime): m_name{name}, m_cost{cost}{
};

Spell::Spell(std::string name, int cost, float castingTime, int damage){
    m_name = name;
    m_cost = cost;
    m_castingTime = castingTime;
    m_minDamage = m_maxDamage = damage;
}

Spell::Spell(std::string name, int cost, float castingTime, int minDamage, int maxDamage){
    m_name = name;
    m_cost = cost;
    m_castingTime = castingTime;
    m_minDamage = minDamage;
    m_maxDamage = maxDamage;
}

void Spell::cast() {
    std::cout << "Casting a spell of '" << m_name << "' on yourself!" << std::endl;
}

void Spell::cast(std::string enemy) {
    std::cout << "Casting a spell of '" << m_name << "' on '" << enemy << "'!"<< std::endl;
}

// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

#include "spell.h"


int main() {
	//Spell empty {}; // приведет к ошибке
    Spell light("Magic Light");
    Spell heal("Healing Hands", 20, 0.5);
    Spell fire("Fireball", 40, 0.2, 50);
    Spell missiles("Arcane Missiles", 30, 0.2, 20, 40);

    light.cast();
    missiles.cast("goblin");

    return 0;
}

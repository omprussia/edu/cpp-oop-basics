// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef SPELL_H_
#define SPELL_H_

#include <iostream>

class Spell {
        private:
            std::string m_name;
            int m_cost {1};
            float m_castingTime {0.1};
            int m_minDamage {1};
            int m_maxDamage {1};
            
        public:
            Spell() = delete;
            Spell(std::string name);
            Spell(std::string name, int cost, float castingTime);
            Spell(std::string name, int cost, float castingTime, int damage);
            Spell(std::string name, int cost, float castingTime, int minDamage, int maxDamage);
            void cast();
            void cast(std::string enemy);
    };

#endif

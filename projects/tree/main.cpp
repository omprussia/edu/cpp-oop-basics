// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include <vector>

template <class T>
class Node {
    private:
        T m_data {};
        std::vector<Node> m_children {};
    public:
        Node() {};
        Node(T value): m_data{value} {}
    
        void addChild(T value) {
            m_children.push_back(Node {value});
        }

        void removeChild(T value) {
            for (int i {}; i < m_children.size(); i++) {
                if (m_children.at(i).m_data == value) {
                    m_children.erase(m_children.begin() + i);
                    return;
                }
            }
        }
        
        void setValue(T newValue) {
            m_data = newValue;
        }
        
        std::vector<Node>& getChildren() {
            return m_children;
        }
        
        void display(int depth = 0) {
            for (int i {}; i < depth; i++) {
                if (i != depth-1) {
                    std::cout << '\t';
                }
                else {
                    std::cout << '\t';
                }
            }
            std::cout << m_data << std::endl;
            for (auto node: m_children) {
                node.display(depth+1);
            }
        }

};

int main() {
    Node<std::string> root {"/"};
    root.addChild("home");
    root.addChild("lib");
    root.addChild("bin");
    
    root.getChildren()[1].addChild("systemd");
    root.getChildren()[1].addChild("udev");
    root.getChildren()[1].addChild("modules");

    root.getChildren()[1].getChildren()[0].addChild("resolv.conf");
    root.getChildren()[1].getChildren()[0].addChild("ntp-units.d");
    
    root.display();
    
    std::cout << std::endl;
    
    root.getChildren()[1].getChildren()[0].display();
}


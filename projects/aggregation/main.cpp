// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

class TemperatureSensor {
    private:
        float m_temperature {22.0};

    public:
        TemperatureSensor(){};
        float getTemperature(){
            return m_temperature;
        };
};


class Heater {
    private:
        TemperatureSensor* m_sensor;
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        Heater(TemperatureSensor* sensor): m_sensor{sensor}{};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }

        void on() {
            m_enabled = true;
            std::cout << "Start heating. Current temperature: " << m_sensor->getTemperature() << std::endl;
        }

        void off() {
            m_enabled = false;
        }
};

class AirConditioner {
    private:
        TemperatureSensor* m_sensor;
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        AirConditioner(TemperatureSensor* sensor): m_sensor{sensor}{};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }

        void on() {
            m_enabled = true;
            std::cout << "Start cooling. Current temperature: " << m_sensor->getTemperature() << std::endl;
        }

        void off() {
            m_enabled = false;
        }
};

int main() {
    TemperatureSensor sensor {};
    Heater heater {&sensor};
    AirConditioner conditioner {&sensor};
    
    heater.on();
    conditioner.on();
    
    return 0;
}


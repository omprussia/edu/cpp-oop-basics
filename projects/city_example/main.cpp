// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

void printCity(char* name, float latitude, float longitude, float area, int population) {
    std::cout << name << " " << latitude << " " << longitude << " ";
    std::cout << area << " " << population << std::endl;
}

int main() {
    char name[5][12] {"Moscow", "Irkutsk", "Tomsk", "Tyumen", "Anadyr"};
    float latitude[5] {55.76, 52.28, 56.5, 57.15, 64.73};
    float longitude[5] {37.62, 104.28, 84.97, 65.53, 177.52};
    float area[5] {2561.5, 277, 294.6, 698, 20};
    int population[5] {13010112, 587891, 524669, 581907, 13045};

    for (int i {}; i<5; i++){
        printCity(name[i], latitude[i], longitude[i], area[i], population[i]);
    }

    return 0;
}

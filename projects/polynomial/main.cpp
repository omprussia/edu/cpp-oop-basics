// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

class Polynomial {
    private:
        double m_a {0.0};
        double m_b {0.0};
        double m_c {0.0};

    public:
        Polynomial(){};
        Polynomial(double a, double b, double c): m_a{a}, m_b{b}, m_c{c} {
        };
        double operator()(double x) {
            return m_a * x * x + m_b * x + m_c;
        }
};

int main() {
    Polynomial poly {2.6, 1.5, 3.2};

    std::cout << poly(2.5) << std::endl;
    std::cout << poly(-1.0) << std::endl;
    std::cout << poly(3.3) << std::endl;
    
    return 0;
}


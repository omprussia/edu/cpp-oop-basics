// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>


int main() // main function
{
    std::cout << "C++ programming language" << std::endl;

    int C = 10;
    std::cout << std::boolalpha << (C > C++) << std::endl;
}

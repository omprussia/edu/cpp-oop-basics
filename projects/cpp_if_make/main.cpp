// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>
#include "lib.h"

int main() {
	std::cout << "Type: " << function() << std::endl;

	return 0;
}

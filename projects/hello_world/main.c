// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <stdio.h>

int main()
{
    printf("C programming language");
    return 0;
}

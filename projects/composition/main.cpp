// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <iostream>

class Heater {
    private:
        float m_temperature {0};
        float m_targetTemperature {0};
        bool m_enabled {false};
    public:
        Heater(){};

        void setTargetTemperature(float temperature) {
            m_targetTemperature = temperature;
        }
        
        int getTemperature() {
            return m_temperature;
        }

        void on() {
            m_enabled = true;
        }

        void off() {
            m_enabled = false;
        }

};

class Light {
    private:
        bool m_enabled {false};

    public:
        Light(){};
    
        void on() {
            m_enabled = true;
        }
    
        void off() {
            m_enabled = false;
        }
};

class HomeAssistant {
    private:
        Light m_light {};
        Heater m_heater {};
    
    public:
        HomeAssistant(){};
        ~HomeAssistant(){
            m_light.off();
            m_heater.off();
        };

        void arriveHome(){
            m_light.on();
            m_heater.setTargetTemperature(24);
            m_heater.on();
        }

        void leaveHome(){
            m_light.off();
            m_heater.setTargetTemperature(18);
        }
    
};

int main() {
    HomeAssistant ha {};
    ha.arriveHome();
    
    return 0;
}

